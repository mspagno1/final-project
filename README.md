Rescue the Princess:
    After a week of building up the courage, our knight is finally going to ask the princess out on a date. But as per usual for the knight, things go terribly wrong when they least expect it. Now our knight must spring into action, to save the princess yet again from the forces of evil.


Rescue the Princess Controls:

Press Above the characters y direction to jump in that direction.This action can only be done on every landing (to prevent flying).

Press below on the same level of the characters y direction to walk in that direction. This action can be done multiple times without landing for increase speed or distance on jumps.

Press the fire button to shoot a fireball ahead of where you are facing. Limited to one fireball, pressing the button will delete any fireball you shoot.

Players health normally would be 30 but for test demo i decided to give 99 so I could get through the demo quickly( stored as a global variable).


Get through the plains area, scale the castle, and defeat the dragon boss who kidnapped the princess.

Bugs:

Note after boss fight do not fall off that platform that spawned (it currently doesn't come back down). Also when reaching princess walk on platform a bit towards princess, the sweet spot is a little too precise.

Also currently only the plains area has a reset button. The others areas should have them but ran out of time (also why there isn't sound =-c)