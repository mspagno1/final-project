//
//  TowerScene.m
//  Rescue the Princess
//
//  Created by Vaster on 5/10/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "TowerScene.h"

static const uint32_t enemyCategory =  0x1 << 1;
static const uint32_t playerCategory =  0x1 << 2;
static const uint32_t groundCategory =  0x1 << 3;
static const uint32_t fireBallCategory = 0x1 << 4;
static const uint32_t backGroundCategory = 0x1 << 5;
static const uint32_t platformCategory = 0x1 << 6;

@implementation TowerScene {
    NSTimeInterval _lastUpdateTime;
    
    //Scenary and level design
    SKSpriteNode * towerWall[5];
    
    SKSpriteNode * startGround;
    
    SKSpriteNode * platform[16];
    
    SKSpriteNode * movingPlatform;
    
    SKSpriteNode * fireButton;
    SKSpriteNode * resetButton;
    SKSpriteNode * leftBorder;
    SKSpriteNode * rightBorder;
    
    //Player
    SKSpriteNode * player;
    
    //Enemies
    SKSpriteNode * harpies[8];
    Boolean harpieDirectLeft[8];
    Boolean harpieAttacking[8];
    Boolean harpieFlying[8];
    
    SKSpriteNode * bats[4];
    Boolean batDirectLeft[6];

    
    //Attacks
    SKSpriteNode * fireBall;
    
    
    //Player actions
    SKAction* playerJumpLeft;
    SKAction* playerWalkLeft;
    SKAction* playerJumpRight;
    SKAction* playerWalkRight;
    SKAction* playerDeath;
    SKAction* playerLaugh;
    SKAction* playerVictory;
    SKAction* playerTalk;
    SKAction* playerCheck;
    
    
    SKTexture* playerHit;
    SKTexture* playerRight;
    SKTexture* playerLeft;
    SKTexture* playerIdle;
    
    
    //Harpie Enemy
    SKAction *harpieFlyL;
    SKAction *harpieFlyR;
    SKAction *harpieDeathL;
    SKAction *harpieDeathR;
    SKAction *harpieAttackR;
    SKAction *harpieAttackL;


    SKTexture *harpieFlyRight[18];
    SKTexture *harpieFlyLeft[18];
    SKTexture *harpieDeathRight[9];
    SKTexture *harpieDeathLeft[9];
    SKTexture *harpieAttackRight[12];
    SKTexture *harpieAttackLeft[12];

    //Bat Enemy
    SKAction *batFlyL;
    SKAction *batFlyR;
    
    SKTexture* batFlyLeft[4];
    SKTexture* batFlyRight[4];

    
    //True is player left false is player is right
    Boolean playerFacingLeft;
    Boolean running;
    Boolean endOfLevel;
    Boolean hitPlatform;
    
    SKCameraNode * cam;
    
    int checkPointX;
    int checkPointY;
    int hitPoints;
    
    //Labels
    SKLabelNode * timeLabel;
    SKLabelNode * defeatLabel;
    SKLabelNode * healthLabel;
    
    //Timeframe counter
    int frameCounter;
    
    
    
}

- (void)sceneDidLoad {
    static BOOL didLoad = NO;
    
    endOfLevel = false;
    hitPlatform = false;
    running = false;
    frameCounter = 0;
    
    for(int i = 0; i < 8; i++){
        harpieAttacking[i] = false;
        harpieFlying[i] = false;
    }
    
    //Scene is being loaded twice for some reason, camera only works on second load
    //LOOKS INTO THIS AT SOME POINT BUT THIS IS A TEMP FIX
    if (didLoad)
    {
        cam = [SKCameraNode node];
        self.camera = cam;
        //NSLog(@"Skipping the second load");
        return;
    }
    didLoad = YES;
    

    
    //Make background
    
    [self makeBackground];
    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );
    self.physicsWorld.contactDelegate = self;
    
    //Make level
    [self levelSetUp];
    
    
    //Make player
    [self makePlayer];
    [self makeAnimation];
    
    //Make enemies
    
    [self makeHarpies];
    [self makeBats];
    [self makeEnemyAnimation];
    
    
    //MakeUI
    [self makeFireButton];
    //[self makeResetButton];
    
    //Make labels
    [self makeHealthLabel];
    
    //Make Borders
    [self makeBorders];
    
    //Checkpoint initial
    checkPointX = player.position.x;
    checkPointY = player.position.y +10;
    
    //Players Health
    //hitPoints =  [[SharedData sharedInstance] playerHealth];
    [[SharedData sharedInstance] setPlayerHealth:10];
    
    //Camera init
    cam.position = CGPointMake(CGRectGetMidX(self.frame), player.position.y + 500);
    
    //Buttons init
    fireButton.position = CGPointMake(0, player.position.y - 100);
    
    //Labels init
    healthLabel.position = CGPointMake(250, 0+550);



    
    
}

-(void) makeBackground{
    for(int i = 0; i < 4; i++){
        towerWall[i] = [SKSpriteNode spriteNodeWithImageNamed: @"Wall.png"];
        
        towerWall[i].size = CGSizeMake(self.frame.size.width,self.frame.size.height);
        
        [towerWall[i] setPosition: CGPointMake(0,0 + self.frame.size.height * i)];
        
        towerWall[i].physicsBody.affectedByGravity = FALSE;
        towerWall[i].physicsBody.categoryBitMask = backGroundCategory;
        [self addChild: (towerWall[i])];
    }
    
}


-(void)makeBorders{
    leftBorder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(20, 4000)];
    
    [leftBorder setPosition: CGPointMake(-385,0)];
    leftBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 10000)];
    leftBorder.physicsBody.dynamic = NO;
    
    leftBorder.physicsBody.affectedByGravity = FALSE;
    leftBorder.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (leftBorder)];
    
    rightBorder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(20, 4000)];
    
    [rightBorder setPosition: CGPointMake(385,0)];
    rightBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 10000)];
    rightBorder.physicsBody.dynamic = NO;
    
    rightBorder.physicsBody.affectedByGravity = FALSE;
    rightBorder.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (rightBorder)];
    
    
}


-(void) makeGround:(int)xLoc :(int)yLoc : (int)xSize{
    startGround  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    startGround.size = CGSizeMake(self.frame.size.width,startGround.texture.size.height + 500);
    
    [startGround setPosition: CGPointMake(0,-518)];
    
    startGround.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 10)];
    startGround.physicsBody.dynamic = NO;
    startGround.physicsBody.affectedByGravity = FALSE;
    startGround.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (startGround)];
}



-(void)levelSetUp{
    [self makeGround:0:-518:3000];
    //MAKE PLATFORMS
    [self makePlatform];
    [self makeMovingPlatform];

    
}

-(void)cleanUp:(int)maxLoc{
    if(startGround.position.x < maxLoc){
        [startGround removeFromParent];
    }
    /*
    for(int i = 0; i < 4; i++){
        if(background[i].position.x < maxLoc - 2000){
            [background[i] removeFromParent];
        }
    }
     */
}

-(void) makePlayer{
    //Set up player
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        playerIdle = [SKTexture textureWithImageNamed:@"Terra.gif"];
    }else{
        playerIdle = [SKTexture textureWithImageNamed:@"Celes.gif"];
    }
    player = [SKSpriteNode spriteNodeWithTexture: playerIdle];
    [player setPosition: CGPointMake(0,-517)];
    player.size = CGSizeMake(80, 100);
    
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    player.physicsBody.categoryBitMask = playerCategory;
    player.physicsBody.contactTestBitMask = enemyCategory;
    player.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self addChild: (player)];
}


-(void) makeHarpies{
    
    //Set up imo
    int harpieYpos[8];
    int harpieXpos[8];
    
    harpieYpos[0] = 4000;
    harpieYpos[1] = 1900;
    harpieYpos[2] = 2100;
    harpieYpos[3] = 2300;
    harpieYpos[4] = 3000;
    harpieYpos[5] = 3200;
    harpieYpos[6] = 3400;
    harpieYpos[7] = 3800;
    
    
    
    int sign;
    int randomNum;
    for(int i = 0; i < 8;i++){
        
        SKTexture* harpieStart;
        
        randomNum = arc4random_uniform(290);
        
        sign = arc4random_uniform(2);
        
        if(sign == 1){
            harpieXpos[i] = randomNum * -1;
            harpieDirectLeft[i] = false;
            harpieStart = [SKTexture textureWithImageNamed:@"harpieFlyRight-1.png"];
            
        }
        else{
            harpieDirectLeft[i] = true;
            harpieXpos[i] = randomNum;
            harpieStart = [SKTexture textureWithImageNamed:@"harpieFlyLeft-1.png"];
            
        }
        
        harpies[i] = [SKSpriteNode spriteNodeWithTexture: harpieStart];
        
        [harpies[i] setPosition: CGPointMake(harpieXpos[i],harpieYpos[i])];
        harpies[i].size = CGSizeMake(80, 100);
        
        [harpies[i] setScale:1.3];
        
        harpies[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(80, 100)];
        harpies[i].physicsBody.dynamic = YES;
        harpies[i].physicsBody.allowsRotation = false;
        harpies[i].physicsBody.affectedByGravity = FALSE;

        harpies[i].physicsBody.categoryBitMask = enemyCategory;
        harpies[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory | platformCategory;
        harpies[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        harpies[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"harpie";
        NSString* harpieNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:harpieNum];
        
        harpies[i].name = str;
        
        [self addChild: (harpies[i])];
        
    }

}

-(void) makeBats{
    
    //Set up imo
    int batsYpos[4];
    int batsXpos[4];
    
    batsYpos[0] = 100;
    batsYpos[1] = -120;
    batsYpos[2] = 700;
    batsYpos[3] = 1500;
  
    
    
    
    int sign;
    int randomNum;
    for(int i = 0; i < 4;i++){
        
        SKTexture* batStart;
        
        randomNum = arc4random_uniform(290);
        
        sign = arc4random_uniform(2);
        
        if(sign == 1){
            batsXpos[i] = randomNum * -1;
            batDirectLeft[i] = false;
            batStart = [SKTexture textureWithImageNamed:@"batFlyRight-0.png"];
            
        }
        else{
            batDirectLeft[i] = true;
            batsXpos[i] = randomNum;
            batStart = [SKTexture textureWithImageNamed:@"batFlyLeft-0.png"];
            
        }
        
        bats[i] = [SKSpriteNode spriteNodeWithTexture: batStart];
        
        [bats[i] setPosition: CGPointMake(batsXpos[i],batsYpos[i])];
        bats[i].size = CGSizeMake(80, 100);
        
        [bats[i] setScale:1.3];
        
        bats[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(80, 100)];
        bats[i].physicsBody.dynamic = YES;
        bats[i].physicsBody.allowsRotation = false;
        bats[i].physicsBody.affectedByGravity = FALSE;
        
        bats[i].physicsBody.categoryBitMask = enemyCategory;
        bats[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory | platformCategory;
        bats[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        bats[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"bat";
        NSString* batNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:batNum];
        
        bats[i].name = str;
        
        [self addChild: (bats[i])];
        
    }
    
}

 -(void) makePlatform {
        for(int i = 0; i < 16; i++){
            platform[i] = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(160, 40)];

            //platform[i] = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor]];
        platform[i].size = CGSizeMake(160, 40);
 
            platform[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(platform[i].size.width, platform[i].size.height)];
                platform[i].physicsBody.dynamic = NO;
                platform[i].physicsBody.affectedByGravity = FALSE;
                platform[i].physicsBody.categoryBitMask = platformCategory;
                platform[i].physicsBody.contactTestBitMask = playerCategory;
                platform[i].physicsBody.usesPreciseCollisionDetection = YES;
        }
        [platform[0] setPosition:CGPointMake(-300, -400)];
        [platform[1] setPosition:CGPointMake(-100, -200)];
        [platform[2] setPosition:CGPointMake(100, 0)];
        [platform[3] setPosition:CGPointMake(200, 200)];
        [platform[4] setPosition:CGPointMake(100, 400)];
        [platform[5] setPosition:CGPointMake(0, 600)];
        [platform[6] setPosition:CGPointMake(300, 800)];
        [platform[7] setPosition:CGPointMake(-200, 1000)];
        [platform[8] setPosition:CGPointMake(300, 1200)];
        [platform[9] setPosition:CGPointMake(100, 1400)];
        [platform[10] setPosition:CGPointMake(-300, 1600)];
        [platform[11] setPosition:CGPointMake(200, 1800)];
        [platform[12] setPosition:CGPointMake(-100, 2000)];
        [platform[13] setPosition:CGPointMake(150, 2200)];
        [platform[14] setPosition:CGPointMake(-150, 2400)];
        [platform[15] setPosition:CGPointMake(-300, 2600)];

 
 
        for(int i = 0; i < 16; i++){
            [self addChild: (platform[i])];
        }
 }

-(void) makeMovingPlatform {
    
    movingPlatform = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(160, 40)];
        
    movingPlatform.size = CGSizeMake(500, 40);
        
    movingPlatform.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(movingPlatform.size.width, movingPlatform.size.height)];
    movingPlatform.physicsBody.dynamic = NO;
    movingPlatform.physicsBody.affectedByGravity = FALSE;
    movingPlatform.physicsBody.categoryBitMask = platformCategory;
    movingPlatform.physicsBody.contactTestBitMask = playerCategory;
    movingPlatform.physicsBody.usesPreciseCollisionDetection = YES;
    
    [movingPlatform setPosition:CGPointMake(0, 2800)];
    
    [self addChild: (movingPlatform)];
    
}



-(void) makeAnimation{
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        //Walk action Right
        SKTexture* TerraWalkRight1 = [SKTexture textureWithImageNamed:@"TerraWalkRight1.gif"];
        TerraWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight2 = [SKTexture textureWithImageNamed:@"TerraWalkRight2.gif"];
        TerraWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight3 = [SKTexture textureWithImageNamed:@"TerraWalkRight3.gif"];
        TerraWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight4 = [SKTexture textureWithImageNamed:@"TerraWalkRight4.gif"];
        TerraWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkRight1,  TerraWalkRight2,TerraWalkRight3,TerraWalkRight4] timePerFrame:0.2]];
        
        SKTexture* TerraWalkLeft1 = [SKTexture textureWithImageNamed:@"TerraWalkLeft1.gif"];
        TerraWalkLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft2 = [SKTexture textureWithImageNamed:@"TerraWalkLeft2.gif"];
        TerraWalkLeft2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft3 = [SKTexture textureWithImageNamed:@"TerraWalkLeft3.gif"];
        TerraWalkLeft3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft4 = [SKTexture textureWithImageNamed:@"TerraWalkLeft4.gif"];
        TerraWalkLeft4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkLeft1,  TerraWalkLeft2,TerraWalkLeft3,TerraWalkLeft4] timePerFrame:0.2]];
        
        playerRight = TerraWalkRight1;
        
        playerLeft = TerraWalkLeft1;
        
        //Jump action Left
        SKTexture* TerraJumpLeft1 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft.gif"];
        TerraJumpLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraJumpLeft2 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft2.gif"];
        TerraJumpLeft2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpLeft1, TerraJumpLeft2] timePerFrame:0.2]];
        
        //Jump action Right
        SKTexture* TerraJumpRight1 = [SKTexture textureWithImageNamed:@"TerraJumpRight1.gif"];
        TerraJumpRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraJumpRight2 = [SKTexture textureWithImageNamed:@"TerraJumpRight2.gif"];
        TerraJumpRight2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpRight1, TerraJumpRight2] timePerFrame:0.2]];
        
        //Victory Wave
        SKTexture* TerraWave1 = [SKTexture textureWithImageNamed:@"TerraWave1.tiff"];
        TerraWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWave2 = [SKTexture textureWithImageNamed:@"TerraWave2.tiff"];
        TerraWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWave1, TerraWave2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* TerraTalk1 = [SKTexture textureWithImageNamed:@"TerraTalkRight1.tiff"];
        TerraTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraTalk2 = [SKTexture textureWithImageNamed:@"TerraTalkRight2.tiff"];
        TerraTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraTalk1, TerraTalk2] timePerFrame:0.2]];
        
        //Laugh
        SKTexture* TerraLaugh1 = [SKTexture textureWithImageNamed:@"TerraLaugh1.tiff"];
        TerraLaugh1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraLaugh2 = [SKTexture textureWithImageNamed:@"TerraLaugh2.tiff"];
        TerraLaugh2.filteringMode = SKTextureFilteringNearest;
        
        playerLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraLaugh1, TerraLaugh2] timePerFrame:0.2]];
        
        //Death
        playerHit = [SKTexture textureWithImageNamed:@"TerraHit1.gif"];
        playerHit.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraHit2 = [SKTexture textureWithImageNamed:@"TerraHit2.gif"];
        TerraHit2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraHit3 = [SKTexture textureWithImageNamed:@"TerraHit3.gif"];
        TerraHit3.filteringMode = SKTextureFilteringNearest;
        
        playerDeath= [SKAction animateWithTextures:@[playerHit, TerraHit2,TerraHit3] timePerFrame:0.5];
        
        //Terra check
        
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }else{
        SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"CelesWalkRight1.tiff"];
        CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"CelesWalkRight2.tiff"];
        CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"CelesWalkRight3.tiff"];
        CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"CelesWalkRight4.tiff"];
        CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,  CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
        
        
        SKTexture* CelesWalkLeft1 = [SKTexture textureWithImageNamed:@"CelesWalkLeft1.tiff"];
        CelesWalkLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft2 = [SKTexture textureWithImageNamed:@"CelesWalkLeft2.tiff"];
        CelesWalkLeft2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft3 = [SKTexture textureWithImageNamed:@"CelesWalkLeft3.tiff"];
        CelesWalkLeft3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft4 = [SKTexture textureWithImageNamed:@"CelesWalkLeft4.tiff"];
        CelesWalkLeft4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkLeft1,  CelesWalkLeft2,CelesWalkLeft3,CelesWalkLeft4] timePerFrame:0.2]];
        
        playerRight = CelesWalkRight1;
        
        playerLeft = CelesWalkLeft1;
        
        //Jump action Left
        SKTexture* CelesJumpLeft1 = [SKTexture textureWithImageNamed:@"CelesJumpLeft1.tiff"];
        CelesJumpLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesJumpLeft2 = [SKTexture textureWithImageNamed:@"CelesJumpLeft2.tiff"];
        CelesJumpLeft2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpLeft1, CelesJumpLeft2] timePerFrame:0.2]];
        
        //Jump action Right
        SKTexture* CelesJumpRight1 = [SKTexture textureWithImageNamed:@"CelesJumpRight1.tiff"];
        CelesJumpRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesJumpRight2 = [SKTexture textureWithImageNamed:@"CelesJumpRight2.tiff"];
        CelesJumpRight2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpRight1, CelesJumpRight2] timePerFrame:0.2]];
        
        //Victory Wave
        SKTexture* CelesWave1 = [SKTexture textureWithImageNamed:@"CelesVictory1.tiff"];
        CelesWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWave2 = [SKTexture textureWithImageNamed:@"CelesVictory2.tiff"];
        CelesWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWave1, CelesWave2] timePerFrame:0.2]];
        
        //Death
        playerHit = [SKTexture textureWithImageNamed:@"CelesDeath1.gif"];
        playerHit.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesDeath1 = [SKTexture textureWithImageNamed:@"CelesDeath2.gif"];
        CelesDeath1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesDeath2 = [SKTexture textureWithImageNamed:@"CelesDeath3.gif"];
        CelesDeath2.filteringMode = SKTextureFilteringNearest;
        
        playerDeath= [SKAction animateWithTextures:@[playerHit, CelesDeath1,CelesDeath2] timePerFrame:0.5];
        
        //Laugh
        SKTexture* CelesLaugh1 = [SKTexture textureWithImageNamed:@"CelesLaugh1.tiff"];
        CelesLaugh1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesLaugh2 = [SKTexture textureWithImageNamed:@"CelesLaugh2.tiff"];
        CelesLaugh2.filteringMode = SKTextureFilteringNearest;
        
        playerLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesLaugh1, CelesLaugh2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* CelesTalk1 = [SKTexture textureWithImageNamed:@"CelesTalk1.tiff"];
        CelesTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesTalk2 = [SKTexture textureWithImageNamed:@"CelesTalk2.tiff"];
        CelesTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesTalk1, CelesTalk2] timePerFrame:0.2]];
        
        //Celes check
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }
    
}

-(void) makeEnemyAnimation{
    //Harpie Animations
    for(int i = 1;i < 18; i++){
        NSString *flyLeftStr = [NSString stringWithFormat:@"harpieFlyLeft-%d.png", i];
        NSString *flyRightStr = [NSString stringWithFormat:@"harpieFlyRight-%d.png", i];

        
        harpieFlyLeft[i] = [SKTexture textureWithImageNamed: flyLeftStr];
        harpieFlyRight[i] = [SKTexture textureWithImageNamed: flyRightStr];
        harpieFlyLeft[i].filteringMode = SKTextureFilteringNearest;
        harpieFlyRight[i].filteringMode = SKTextureFilteringNearest;


    }
    harpieFlyL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieFlyLeft[1],harpieFlyLeft[2],harpieFlyLeft[3],harpieFlyLeft[4],harpieFlyLeft[5],harpieFlyLeft[6],harpieFlyLeft[7],harpieFlyLeft[8],harpieFlyLeft[9],harpieFlyLeft[10],harpieFlyLeft[11],harpieFlyLeft[12],harpieFlyLeft[13],harpieFlyLeft[14],harpieFlyLeft[15],harpieFlyLeft[16],harpieFlyLeft[17]] timePerFrame:0.2 resize:false restore:false]];
    harpieFlyR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieFlyRight[1],harpieFlyRight[2],harpieFlyRight[3],harpieFlyRight[4],harpieFlyRight[5],harpieFlyRight[6],harpieFlyRight[7],harpieFlyRight[8],harpieFlyRight[9],harpieFlyRight[10],harpieFlyRight[11],harpieFlyRight[12],harpieFlyRight[13],harpieFlyRight[14],harpieFlyRight[15],harpieFlyRight[16],harpieFlyRight[17]] timePerFrame:0.2 resize:false restore:false]];
    
    for(int i = 1;i < 9; i++){
        NSString *deathLeftStr = [NSString stringWithFormat:@"harpieDeathLeft-%d.png", i];
        NSString *deathRightStr = [NSString stringWithFormat:@"harpieDeathRight-%d.png", i];
        
        
        harpieDeathLeft[i] = [SKTexture textureWithImageNamed: deathLeftStr];
        harpieDeathRight[i] = [SKTexture textureWithImageNamed: deathRightStr];
        harpieDeathLeft[i].filteringMode = SKTextureFilteringNearest;
        harpieDeathRight[i].filteringMode = SKTextureFilteringNearest;


        
    }
    harpieDeathL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieDeathLeft[1],harpieDeathLeft[2],harpieDeathLeft[3],harpieDeathLeft[4],harpieDeathLeft[5],harpieDeathLeft[6],harpieDeathLeft[7],harpieDeathLeft[8]] timePerFrame:0.2 resize:false restore:false]];
    
    harpieDeathR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieDeathRight[1],harpieDeathRight[2],harpieDeathRight[3],harpieDeathRight[4],harpieDeathRight[5],harpieDeathRight[6],harpieDeathRight[7],harpieDeathRight[8]] timePerFrame:0.2 resize:false restore:false]];

    
    for(int i = 1;i < 11; i++){
        NSString *attackLeftStr = [NSString stringWithFormat:@"harpieAttackLeft-%d.png", i];
        NSString *attackRightStr = [NSString stringWithFormat:@"harpieAttackRight-%d.png", i];
        
        
        harpieAttackLeft[i] = [SKTexture textureWithImageNamed: attackLeftStr];
        harpieAttackRight[i] = [SKTexture textureWithImageNamed: attackRightStr];
        
        harpieAttackLeft[i].filteringMode = SKTextureFilteringNearest;

        harpieAttackRight[i].filteringMode = SKTextureFilteringNearest;

        
    }
    
    harpieAttackL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieAttackLeft[1],harpieAttackLeft[2],harpieAttackLeft[3],harpieAttackLeft[4],harpieAttackLeft[5],harpieAttackLeft[6],harpieAttackLeft[7],harpieAttackLeft[8],harpieAttackLeft[9],harpieAttackLeft[10]] timePerFrame:0.2 resize:false restore:false]];
    
    harpieAttackR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieAttackRight[1],harpieAttackRight[2],harpieAttackRight[3],harpieAttackRight[4],harpieAttackRight[5],harpieAttackRight[6],harpieAttackRight[7],harpieAttackRight[8],harpieAttackRight[9],harpieAttackRight[10]] timePerFrame:0.2 resize:false restore:false]];
    
    //Bat
    
    for(int i = 0;i < 4; i++){
        NSString *flyLeft = [NSString stringWithFormat:@"batFlyLeft-%d.png", i];
        NSString *flyRight= [NSString stringWithFormat:@"batFlyRight-%d.png", i];
        
        
        batFlyLeft[i] = [SKTexture textureWithImageNamed: flyLeft];
        batFlyRight[i] = [SKTexture textureWithImageNamed: flyRight];
        
    }
    batFlyL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[batFlyLeft[0],batFlyLeft[1],batFlyLeft[2],batFlyLeft[3]] timePerFrame:0.2]];
    
    batFlyR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[batFlyRight[0],batFlyRight[1],batFlyRight[2],batFlyRight[3]] timePerFrame:0.2]];
    

}

-(void)makeFireBall{
    [fireBall removeFromParent];
    fireBall = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
    //Fireball to the left or right of the player
    if(playerFacingLeft == false){
        [fireBall setPosition: CGPointMake(player.position.x + 30,player.position.y +10)];
    }
    else{
        [fireBall setPosition: CGPointMake(player.position.x - 30,player.position.y + 10)];
    }
    fireBall.size = CGSizeMake(60, 60);
    
    fireBall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fireBall.texture.size];
    
    fireBall.physicsBody.affectedByGravity = false;
    fireBall.physicsBody.allowsRotation = false;
    fireBall.physicsBody.categoryBitMask = fireBallCategory;
    fireBall.physicsBody.contactTestBitMask = enemyCategory;
    fireBall.physicsBody.usesPreciseCollisionDetection = YES;
    [self addChild: fireBall];
}


//fire button
- (void)makeFireButton
{
    fireButton = [SKSpriteNode spriteNodeWithImageNamed:@"fireButton.png"];
    fireButton.size = CGSizeMake(100 ,100);
    fireButton.position = CGPointMake(player.position.x,-620);
    fireButton.name = @"fireButtonNode";//how the node is identified later
    
    [self addChild: (fireButton)];
    
}

 
 -(void) makeHealthLabel{
    healthLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    healthLabel.position = CGPointMake(player.position.x+300, 600);
    healthLabel.fontSize = 50;
    healthLabel.fontColor = [SKColor redColor];
    healthLabel.name = @"healthLabel";
 
    NSString *hpLeft = [NSString stringWithFormat:@"HP : %d",hitPoints];
    healthLabel.text = hpLeft;
 
    [self addChild:healthLabel];
 
 }
 
-(void) makeResetButton {
    resetButton = [SKSpriteNode spriteNodeWithImageNamed:@"reset.png"];
    resetButton.size = CGSizeMake(100 ,100);
    resetButton.position = CGPointMake(player.position.x + 270,-620);
    resetButton.name = @"reset";//how the node is identified later
    
    [self addChild: (resetButton)];
}


-(void) makeDefeatLabel{
    defeatLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    defeatLabel.position = CGPointMake(player.position.x, player.position.y+400);
    defeatLabel.fontSize = 40;
    defeatLabel.fontColor = [SKColor redColor];
    defeatLabel.name = @"defeatLabel";
    
    NSString * gameOverText = [NSString stringWithFormat:@"You failed to save the princess!"];
    defeatLabel.text = gameOverText;
    
    [self addChild:defeatLabel];
    
    
}

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask == enemyCategory)&&
        (secondBody.categoryBitMask == enemyCategory))
    {
        //Figure out how to unclog imps
    }
    
    if ((firstBody.categoryBitMask == enemyCategory)&& (secondBody.categoryBitMask == fireBallCategory))
    {
        // printf("Fireball hit");
        //NSLog(@"%@ \n" , firstBody.node.name);
        //How know what object to remove
        if([firstBody.node.name  isEqualToString: @"harpie0"]){
            [harpies[0] removeAllActions];
            [harpies[0] runAction:harpieDeathL];
            [harpies[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"harpie1"]){
            [harpies[1] removeAllActions];
            [harpies[1] runAction:harpieDeathL];
            [harpies[1] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"harpie2"]){
            [harpies[2] removeAllActions];
            [harpies[2] runAction:harpieDeathL];
            [harpies[2] removeFromParent];
            
        }
        
        if([firstBody.node.name  isEqualToString: @"harpie3"]){
            [harpies[3] removeAllActions];
            [harpies[3] runAction:harpieDeathL];
            [harpies[3] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"harpie4"]){
            [harpies[4] removeAllActions];
            [harpies[4] runAction:harpieDeathL];
            [harpies[4] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"harpie5"]){
            [harpies[5] removeAllActions];
            [harpies[5] runAction:harpieDeathL];
            [harpies[5] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"harpie6"]){
            [harpies[6] removeAllActions];
            [harpies[6] runAction:harpieDeathL];
            [harpies[6] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"harpie7"]){
            [harpies[7] removeAllActions];
            [harpies[7] runAction:harpieDeathL];
            [harpies[7] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"bat0"]){
            [bats[0] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"bat1"]){
            [bats[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bat2"]){
            [bats[2] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"bat3"]){
            [bats[3] removeFromParent];
        }
        
        
        
        //Change to dead imp
        [fireBall removeFromParent];
        
        
    }
    
    if ((firstBody.categoryBitMask == enemyCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        //WHY IS THIS BREAKING NOT SURE
        //printf("%@ %@ \n", firstBody, secondBody);
        //NSLog(@" %@ %@ \n", firstBody, secondBody);
        //printf("Collison");
        hitPoints --;
        
        if (hitPoints > 0){
            [player removeAllActions];
            [player setTexture: playerHit];
        }
        
    }
    
    
    [[SharedData sharedInstance] setPlayerHealth:hitPoints];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    
    if ([node.name isEqualToString:@"fireButtonNode"] && hitPoints > 0) {
        [self makeFireBall];
        if(playerFacingLeft == true){
            [fireBall.physicsBody applyImpulse:CGVectorMake(-15, 0)];
        }
        else{
            [fireBall.physicsBody applyImpulse:CGVectorMake(15, 0)];
        }
    }
    
    else if ([node.name isEqualToString:@"reset"]) {
        //[self reset];
    }
    
    else{
        
        if(hitPoints > 0){
            if(location.y <= player.position.y){
                //if(player.physicsBody.velocity.dx == 0){
                if(location.x > player.position.x && location.y){
                    [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
                    if(running == false || playerFacingLeft == true){
                        [player runAction:playerWalkRight];
                    }
                    running = true;
                    playerFacingLeft = false;
                    
                }
                else{
                    [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                    if(running == false || playerFacingLeft == false){
                        [player runAction:playerWalkLeft];
                    }
                    running = true;
                    
                    playerFacingLeft = true;
                    
                }
            }
            else{
                //Enter cheat here if take too long
                if(player.physicsBody.velocity.dy == 0){
                    if(location.x > player.position.x && location.y){
                        playerFacingLeft = false;
                        
                        [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
                        [player runAction:playerJumpRight];
                        running = false;
                        
                    }
                    else{
                        playerFacingLeft = true;
                        [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                        [player runAction:playerJumpLeft];
                        running = false;
                        
                    }
                }
                //Cheat here
            }
            
        }
        
    }
    
}

-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    hitPoints = [[SharedData sharedInstance] playerHealth];
    
    if(hitPoints >= 0){
        NSString *hpLeft = [NSString stringWithFormat:@"HP : %d",hitPoints];
        healthLabel.text = hpLeft;
    }
    
    if(player.position.y > 0 && player.position.y < 3500){
        cam.position = CGPointMake(CGRectGetMidX(self.frame), player.position.y);
        fireButton.position = CGPointMake(0, player.position.y - 500);
        resetButton.position = CGPointMake(player.position.x + 270, -620);
        healthLabel.position = CGPointMake(250, player.position.y+550);


    }
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    printf("Player x : %f y : %f \n",player.position.x,player.position.y);
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    if(player.physicsBody.velocity.dx == 0 && player.physicsBody.velocity.dy == 0 && hitPoints > 0){
        [player removeAllActions];
        if(player.position.x < 20 && player.position.x > -20){
            [player setTexture: playerIdle];
            running = false;
        }
        else{
            if(playerFacingLeft == true){
                running = false;
                [player setTexture: playerLeft];
            }else{
                running = false;
                [player setTexture:playerRight];
            }
        }
    }
    
    
    
     if(hitPoints == 0){
        [player removeAllActions];
        [player runAction:playerDeath];
        hitPoints = -1;
     }
    
    
    //Moving platform check here
    if(player.position.y > movingPlatform.position.y + 10 && player.position.x > movingPlatform.position.x-200 && hitPlatform == false){
        hitPlatform = true;
        [movingPlatform runAction:[SKAction moveTo:CGPointMake(0,4300) duration: 10]];

    }
    if(player.position.y > 4200){
        [player removeAllActions];
        [player runAction:playerLaugh];
    }
    
    if(player.position.y > 4300){
        GKScene *scene = [GKScene sceneWithFileNamed:@"BossScene"];
        
        SKTransition *reveal = [SKTransition fadeWithDuration:1.5];
        BossScene * bossScene = (BossScene *)scene.rootNode;
        
        bossScene.entities = [scene.entities mutableCopy];
        bossScene.graphs = [scene.graphs mutableCopy];
        bossScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:bossScene transition: reveal];
        
    }
    
    //Enenmy movements here
    //Harpie movement
    for(int i = 0; i < 8; i++){
        if(harpies[i].physicsBody.velocity.dx == 0 && harpieDirectLeft[i] == true){
            //[harpies[i] runAction:[SKAction moveTo:CGPointMake(-300,harpies[i].position.y) duration: 5]];
            [harpies[i].physicsBody applyImpulse:CGVectorMake(-50, 0)];
            //if(harpieFlying[i] == false){
                [harpies[i] runAction:harpieFlyL];
                harpieFlying[i] = true;
                harpieAttacking[i] = false;

            //}
            //harpieDirectLeft[i] = false;

        }
        else if(harpies[i].physicsBody.velocity.dx == 0 && harpieDirectLeft[i] == false){
            //[harpies[i] runAction:[SKAction moveTo:CGPointMake(300,harpies[i].position.y) duration: 5]];
            [harpies[i].physicsBody applyImpulse:CGVectorMake(50, 0)];
            //if(harpieFlying[i] == false){
                [harpies[i] runAction:harpieFlyR];
                harpieFlying[i] = true;
                harpieAttacking[i] = false;

            //}
            
            //harpieDirectLeft[i] = true;
            
        }
        
        if(harpies[i].position.x <= -290){
            harpieDirectLeft[i] = false;
        }
        if(harpies[i].position.x >= 290){
            harpieDirectLeft[i] = true;
        }
        
        if(harpieDirectLeft[i] == true){
            if(player.position.x > harpies[i].position.x - 150 && player.position.y > harpies[i].position.y - 200){
                if(harpieAttacking[i] == false){
                    [harpies[i] runAction:harpieAttackL];
                }
                
                harpieAttacking[i] = true;
                harpieFlying[i] =false;

            }else{
                if(harpieFlying[i] == false){
                    [harpies[i] runAction:harpieFlyL];
                    harpieFlying[i] =true;
                }
                harpieAttacking[i] = false;


            }
        }
        if(harpieDirectLeft[i] == false){
            if(player.position.x < harpies[i].position.x + 300 && player.position.y > harpies[i].position.y - 200){
                if(harpieAttacking[i] == false){
                    [harpies[i] runAction:harpieAttackR];
                }
                
                harpieAttacking[i] = true;
                harpieFlying[i] =false;

            }else{
                if(harpieFlying[i] == false){
                    [harpies[i] runAction:harpieFlyR];
                    harpieFlying[i] =true;
                }
                harpieAttacking[i] =false;

            }
        }
        

        
    }
    for(int i = 0; i < 4; i++){
        if(bats[i].physicsBody.velocity.dx == 0 && batDirectLeft[i] == true){
            [bats[i].physicsBody applyImpulse:CGVectorMake(-50, 0)];
            [bats[i] runAction:batFlyR];
            
        }
        else if(bats[i].physicsBody.velocity.dx == 0 && batDirectLeft[i] == false){
            //[batss[i] runAction:[SKAction moveTo:CGPointMake(300,batss[i].position.y) duration: 5]];
            [bats[i].physicsBody applyImpulse:CGVectorMake(50, 0)];
            
            [bats[i] runAction:batFlyL];
        }
        
        if(bats[i].position.x <= -290){
            batDirectLeft[i] = false;
        }
        if(bats[i].position.x >= 290){
            batDirectLeft[i] = true;
        }
        
        
        
    }
    
    
    _lastUpdateTime = currentTime;
    frameCounter++;
}
@end
