//
//  BossScene.m
//  Rescue the Princess
//
//  Created by Vaster on 5/10/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "BossScene.h"

static const uint32_t enemyProjCategory = 0x1 << 0;
static const uint32_t enemyCategory =  0x1 << 1;
static const uint32_t playerCategory =  0x1 << 2;
static const uint32_t groundCategory =  0x1 << 3;
static const uint32_t fireBallCategory = 0x1 << 4;
static const uint32_t backGroundCategory = 0x1 << 5;
static const uint32_t platformCategory = 0x1 << 6;

@implementation BossScene {
    NSTimeInterval _lastUpdateTime;
    
    //Scenary and level design
    SKSpriteNode * wall;
    SKSpriteNode * towerTop;
    
    SKSpriteNode * ground;
    SKSpriteNode * platToPrincess;
    SKSpriteNode * platform[2];
    SKSpriteNode * princessPlat;
    Boolean platUp[2];
    
    
    SKSpriteNode * fireButton;
    
    SKSpriteNode * resetButton;
    SKSpriteNode * leftBorder;
    SKSpriteNode * rightBorder;
    
    //Player
    SKSpriteNode * player;
    
    //Princess
    SKSpriteNode * princess;
    
    //Enemies
    //Boss minion harpies
    SKSpriteNode * harpies[4];
    Boolean harpiesAlive[4];
    int harpieHP[4];
    Boolean harpieDirectLeft[8];
    Boolean harpieAttacking[8];
    Boolean harpieFlying[8];
    
    //Harpie Enemy
    SKAction *harpieFlyL;
    SKAction *harpieFlyR;
    SKAction *harpieDeathL;
    SKAction *harpieDeathR;
    SKAction *harpieAttackR;
    SKAction *harpieAttackL;
    
    
    SKTexture *harpieFlyRight[18];
    SKTexture *harpieFlyLeft[18];
    SKTexture *harpieDeathRight[9];
    SKTexture *harpieDeathLeft[9];
    SKTexture *harpieAttackRight[12];
    SKTexture *harpieAttackLeft[12];
    
    
    //Boss
    SKSpriteNode * dragonBoss;
    
    //Boss Actions
    SKTexture * bossTiltRightUp;
    SKTexture * bossTiltRightDown;
    SKTexture * bossRight;
    
    SKTexture * bossTiltLeftUp;
    SKTexture * bossTiltLeftDown;
    SKTexture * bossLeft;
    
    
    //Attacks
    SKSpriteNode * fireBall;
    SKSpriteNode * bossFire[3];
    
    
    //Player actions
    SKAction* playerJumpLeft;
    SKAction* playerWalkLeft;
    SKAction* playerJumpRight;
    SKAction* playerWalkRight;
    SKAction* playerDeath;
    SKAction* playerLaugh;
    SKAction* playerVictory;
    SKAction* playerTalk;
    SKAction* playerCheck;
    
    
    SKTexture* playerHit;
    SKTexture* playerRight;
    SKTexture* playerLeft;
    SKTexture* playerIdle;
    
    //Princess actions
    
    SKAction * mariaWalkRight;
    SKAction * mariaWalkLeft;
    SKAction * mariaSing;
    
    SKTexture * mariaIdle;
    SKTexture * mariaRight;
    SKTexture * mariaLeft;
    
    //Boss Actions
    
    //True is player left false is player is right
    Boolean playerFacingLeft;
    Boolean running;
    Boolean harpiesSpawn;
    Boolean hitPlatform;
    Boolean minionCheck;
    
    SKCameraNode * cam;
    
    int hitPoints;
    int bossHealth;
    
    //Labels
    SKLabelNode * victoryLabel;
    SKLabelNode * defeatLabel;
    SKLabelNode * healthLabel;
    
    //Timeframe counter
    int frameCounter;
    
    
    
}

- (void)sceneDidLoad {
    static BOOL didLoad = NO;
    
    harpiesSpawn = false;
    hitPlatform = false;
    running = false;
    minionCheck = true;
    frameCounter = 0;
    
    //Scene is being loaded twice for some reason, camera only works on second load
    //LOOKS INTO THIS AT SOME POINT BUT THIS IS A TEMP FIX
    if (didLoad)
    {
        cam = [SKCameraNode node];
        self.camera = cam;
        //NSLog(@"Skipping the second load");
        return;
    }
    didLoad = YES;
    
    
    
    //Make background
    
    [self makeBackground];
    [self makeTower];
    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );
    self.physicsWorld.contactDelegate = self;
    
    //Make level
    [self levelSetUp];
    
    
    //Make player
    [self makePlayer];
    [self makeAnimation];

    
    //Make princess
    [self makePrincess];
    [self makePrincessAnimation];
    
    //Boss
    [self makeBoss];
    [self makeBossAnimation];
    [self makeEnemyAnimation];
    
    //MakeUI
    [self makeFireButton];
    //[self makeResetButton];
    
    //Make labels
    [self makeHealthLabel];
    
    //Make Borders
    [self makeBorders];
    

    
    //Players Health
    hitPoints =  [[SharedData sharedInstance] playerHealth];
    if(hitPoints== 0){
        hitPoints = 20;
        [[SharedData sharedInstance] setPlayerHealth:hitPoints];

    }
    bossHealth = 20;
    
    //Camera init
    cam.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    //Buttons init
    fireButton.position = CGPointMake(0, player.position.y - 100);
    
    //Labels init
    healthLabel.position = CGPointMake(250, 0+550);
    
}


-(void) makeBackground{
    wall = [SKSpriteNode spriteNodeWithImageNamed: @"Wall.png"];
    wall.size = CGSizeMake(self.frame.size.width,self.frame.size.height);
    [wall setPosition: CGPointMake(0,0)];
    wall.physicsBody.affectedByGravity = FALSE;
    wall.physicsBody.categoryBitMask = backGroundCategory;
    [self addChild: (wall)];
    
    
}

-(void) makeTower{
    towerTop = [SKSpriteNode spriteNodeWithImageNamed: @"TowerTop.png"];
    towerTop.size = CGSizeMake(self.frame.size.width,self.frame.size.height);
    [towerTop setPosition: CGPointMake(0,0+self.frame.size.height)];
    towerTop.physicsBody.affectedByGravity = FALSE;
    towerTop.physicsBody.categoryBitMask = backGroundCategory;
    [self addChild: (towerTop)];
    
    princessPlat  = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(350, 40)];
    princessPlat.size = CGSizeMake(350, 40);
    
    [princessPlat setPosition: CGPointMake(20,1340)];
    
    princessPlat.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(350, 40)];
    princessPlat.physicsBody.dynamic = NO;
    princessPlat.physicsBody.affectedByGravity = FALSE;
    princessPlat.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (princessPlat)];
}



-(void)makeBorders{
    leftBorder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(20, 4000)];
    
    [leftBorder setPosition: CGPointMake(-385,0)];
    leftBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 10000)];
    leftBorder.physicsBody.dynamic = NO;
    
    leftBorder.physicsBody.affectedByGravity = FALSE;
    leftBorder.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (leftBorder)];
    
    rightBorder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(20, 4000)];
    
    [rightBorder setPosition: CGPointMake(385,0)];
    rightBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 10000)];
    rightBorder.physicsBody.dynamic = NO;
    
    rightBorder.physicsBody.affectedByGravity = FALSE;
    rightBorder.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (rightBorder)];
    
    
}


-(void) makeGround{
    ground  = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(self.frame.size.width, 170)];
    ground.size = CGSizeMake(self.frame.size.width, 170);
    
    [ground setPosition: CGPointMake(0,-650)];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 230)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    ground.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (ground)];
}



-(void)levelSetUp{
    [self makeGround];
    [self makePlatform];
}


-(void) makePlayer{
    //Set up player
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        playerIdle = [SKTexture textureWithImageNamed:@"Terra.gif"];
    }else{
        playerIdle = [SKTexture textureWithImageNamed:@"Celes.gif"];
    }
    player = [SKSpriteNode spriteNodeWithTexture: playerIdle];
    [player setPosition: CGPointMake(0,-517)];
    player.size = CGSizeMake(80, 100);
    
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    player.physicsBody.categoryBitMask = playerCategory;
    player.physicsBody.contactTestBitMask = enemyCategory|enemyProjCategory;
    player.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self addChild: (player)];
}
-(void) makePrincess{
    //Set up player
    mariaIdle = [SKTexture textureWithImageNamed:@"Maria.gif"];
    princess = [SKSpriteNode spriteNodeWithTexture: mariaIdle];
    [princess setPosition: CGPointMake(100,1360)];
    princess.size = CGSizeMake(80, 100);
    
    princess.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:princess.texture.size];
    princess.physicsBody.dynamic = YES;
    princess.physicsBody.allowsRotation = false;
    
    [self addChild: (princess)];
}

-(void) makeBoss{
    //Set up player
    SKTexture *bossSpawn = [SKTexture textureWithImageNamed:@"BossLeft.gif"];
    dragonBoss = [SKSpriteNode spriteNodeWithTexture: bossSpawn];
    [dragonBoss setPosition: CGPointMake(250,-450)];
    dragonBoss.size = CGSizeMake(400, 300);
    
    dragonBoss.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:dragonBoss.texture.size];
    dragonBoss.physicsBody.dynamic = YES;
    dragonBoss.physicsBody.allowsRotation = false;
    dragonBoss.physicsBody.affectedByGravity = FALSE;
    dragonBoss.physicsBody.categoryBitMask = enemyCategory;
    dragonBoss.physicsBody.contactTestBitMask = playerCategory|fireBallCategory;
    dragonBoss.physicsBody.usesPreciseCollisionDetection = YES;
    
    //printf("Times im running \n");
    
    NSString *str = @"dragonBoss";
    dragonBoss.name = str;
    
    
    [self addChild: (dragonBoss)];
}

-(void) makeHarpies{
    
    //Set up imo
    int harpieYpos[4];
    int harpieXpos[4];
    
    harpieYpos[0] = -517;
    harpieYpos[1] = -300;
    harpieYpos[2] = 0;
    harpieYpos[3] = 200;

    
    
    
    int sign;
    int randomNum;
    for(int i = 0; i < 4;i++){
        
        SKTexture* harpieStart;
        
        randomNum = arc4random_uniform(290);
        
        sign = arc4random_uniform(2);
        
        if(sign == 1){
            harpieXpos[i] = randomNum * -1;
            harpieDirectLeft[i] = false;
            harpieStart = [SKTexture textureWithImageNamed:@"harpieFlyRight-1.png"];
            
        }
        else{
            harpieDirectLeft[i] = true;
            harpieXpos[i] = randomNum;
            harpieStart = [SKTexture textureWithImageNamed:@"harpieFlyLeft-1.png"];
            
        }
        
        harpies[i] = [SKSpriteNode spriteNodeWithTexture: harpieStart];
        
        [harpies[i] setPosition: CGPointMake(harpieXpos[i],harpieYpos[i])];
        harpies[i].size = CGSizeMake(80, 100);
        
        [harpies[i] setScale:1.3];
        
        harpies[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(80, 100)];
        harpies[i].physicsBody.dynamic = YES;
        harpies[i].physicsBody.allowsRotation = false;
        harpies[i].physicsBody.affectedByGravity = FALSE;
        
        harpies[i].physicsBody.categoryBitMask = enemyCategory;
        harpies[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory | platformCategory;
        harpies[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        harpies[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"harpie";
        NSString* harpieNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:harpieNum];
        
        harpies[i].name = str;
        
        [self addChild: (harpies[i])];
        harpieHP[i] = 3;
        harpiesAlive[i] = true;
        
    }
    
}




-(void) platToPrincess{
    platToPrincess = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(160, 40)];
    
    platToPrincess.size = CGSizeMake(160, 40);
    
    platToPrincess.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(platToPrincess.size.width, platToPrincess.size.height)];
    platToPrincess.physicsBody.dynamic = NO;
    platToPrincess.physicsBody.affectedByGravity = FALSE;
    platToPrincess.physicsBody.categoryBitMask = platformCategory;
    platToPrincess.physicsBody.contactTestBitMask = playerCategory;
    platToPrincess.physicsBody.usesPreciseCollisionDetection = YES;
    
    [platToPrincess setPosition:CGPointMake(-300, -400)];
    
    [self addChild: (platToPrincess)];
}

-(void) makePlatform {
    
    for(int i = 0; i < 1; i++){
        platform[i] = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(160, 40)];
        platform[i].size = CGSizeMake(160, 40);
        
        platform[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(platform[i].size.width, platform[i].size.height)];
        platform[i].physicsBody.dynamic = NO;
        platform[i].physicsBody.affectedByGravity = FALSE;
        platform[i].physicsBody.categoryBitMask = platformCategory;
        platform[i].physicsBody.contactTestBitMask = playerCategory;
        platform[i].physicsBody.usesPreciseCollisionDetection = YES;
    }
    [platform[0] setPosition:CGPointMake(-300, -400)];
    //[platform[1] setPosition:CGPointMake(300, 200)];

    for(int i = 0; i < 1; i++){
        [self addChild: (platform[i])];
    }
    platUp[0] = true;
    //platUp[1] = false;

}

-(void) makeAnimation{
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        //Walk action Right
        SKTexture* TerraWalkRight1 = [SKTexture textureWithImageNamed:@"TerraWalkRight1.gif"];
        TerraWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight2 = [SKTexture textureWithImageNamed:@"TerraWalkRight2.gif"];
        TerraWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight3 = [SKTexture textureWithImageNamed:@"TerraWalkRight3.gif"];
        TerraWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight4 = [SKTexture textureWithImageNamed:@"TerraWalkRight4.gif"];
        TerraWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkRight1,  TerraWalkRight2,TerraWalkRight3,TerraWalkRight4] timePerFrame:0.2]];
        
        SKTexture* TerraWalkLeft1 = [SKTexture textureWithImageNamed:@"TerraWalkLeft1.gif"];
        TerraWalkLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft2 = [SKTexture textureWithImageNamed:@"TerraWalkLeft2.gif"];
        TerraWalkLeft2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft3 = [SKTexture textureWithImageNamed:@"TerraWalkLeft3.gif"];
        TerraWalkLeft3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft4 = [SKTexture textureWithImageNamed:@"TerraWalkLeft4.gif"];
        TerraWalkLeft4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkLeft1,  TerraWalkLeft2,TerraWalkLeft3,TerraWalkLeft4] timePerFrame:0.2]];
        
        playerRight = TerraWalkRight1;
        
        playerLeft = TerraWalkLeft1;
        
        //Jump action Left
        SKTexture* TerraJumpLeft1 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft.gif"];
        TerraJumpLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraJumpLeft2 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft2.gif"];
        TerraJumpLeft2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpLeft1, TerraJumpLeft2] timePerFrame:0.2]];
        
        //Jump action Right
        SKTexture* TerraJumpRight1 = [SKTexture textureWithImageNamed:@"TerraJumpRight1.gif"];
        TerraJumpRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraJumpRight2 = [SKTexture textureWithImageNamed:@"TerraJumpRight2.gif"];
        TerraJumpRight2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpRight1, TerraJumpRight2] timePerFrame:0.2]];
        
        //Victory Wave
        SKTexture* TerraWave1 = [SKTexture textureWithImageNamed:@"TerraWave1.tiff"];
        TerraWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWave2 = [SKTexture textureWithImageNamed:@"TerraWave2.tiff"];
        TerraWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWave1, TerraWave2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* TerraTalk1 = [SKTexture textureWithImageNamed:@"TerraTalkRight1.tiff"];
        TerraTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraTalk2 = [SKTexture textureWithImageNamed:@"TerraTalkRight2.tiff"];
        TerraTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraTalk1, TerraTalk2] timePerFrame:0.2]];
        
        //Laugh
        SKTexture* TerraLaugh1 = [SKTexture textureWithImageNamed:@"TerraLaugh1.tiff"];
        TerraLaugh1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraLaugh2 = [SKTexture textureWithImageNamed:@"TerraLaugh2.tiff"];
        TerraLaugh2.filteringMode = SKTextureFilteringNearest;
        
        playerLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraLaugh1, TerraLaugh2] timePerFrame:0.2]];
        
        //Death
        playerHit = [SKTexture textureWithImageNamed:@"TerraHit1.gif"];
        playerHit.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraHit2 = [SKTexture textureWithImageNamed:@"TerraHit2.gif"];
        TerraHit2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraHit3 = [SKTexture textureWithImageNamed:@"TerraHit3.gif"];
        TerraHit3.filteringMode = SKTextureFilteringNearest;
        
        playerDeath= [SKAction animateWithTextures:@[playerHit, TerraHit2,TerraHit3] timePerFrame:0.5];
        
        //Terra check
        
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }else{
        SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"CelesWalkRight1.tiff"];
        CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"CelesWalkRight2.tiff"];
        CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"CelesWalkRight3.tiff"];
        CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"CelesWalkRight4.tiff"];
        CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,  CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
        
        
        SKTexture* CelesWalkLeft1 = [SKTexture textureWithImageNamed:@"CelesWalkLeft1.tiff"];
        CelesWalkLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft2 = [SKTexture textureWithImageNamed:@"CelesWalkLeft2.tiff"];
        CelesWalkLeft2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft3 = [SKTexture textureWithImageNamed:@"CelesWalkLeft3.tiff"];
        CelesWalkLeft3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft4 = [SKTexture textureWithImageNamed:@"CelesWalkLeft4.tiff"];
        CelesWalkLeft4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkLeft1,  CelesWalkLeft2,CelesWalkLeft3,CelesWalkLeft4] timePerFrame:0.2]];
        
        playerRight = CelesWalkRight1;
        
        playerLeft = CelesWalkLeft1;
        
        //Jump action Left
        SKTexture* CelesJumpLeft1 = [SKTexture textureWithImageNamed:@"CelesJumpLeft1.tiff"];
        CelesJumpLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesJumpLeft2 = [SKTexture textureWithImageNamed:@"CelesJumpLeft2.tiff"];
        CelesJumpLeft2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpLeft1, CelesJumpLeft2] timePerFrame:0.2]];
        
        //Jump action Right
        SKTexture* CelesJumpRight1 = [SKTexture textureWithImageNamed:@"CelesJumpRight1.tiff"];
        CelesJumpRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesJumpRight2 = [SKTexture textureWithImageNamed:@"CelesJumpRight2.tiff"];
        CelesJumpRight2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpRight1, CelesJumpRight2] timePerFrame:0.2]];
        
        //Victory Wave
        SKTexture* CelesWave1 = [SKTexture textureWithImageNamed:@"CelesVictory1.tiff"];
        CelesWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWave2 = [SKTexture textureWithImageNamed:@"CelesVictory2.tiff"];
        CelesWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWave1, CelesWave2] timePerFrame:0.2]];
        
        //Death
        playerHit = [SKTexture textureWithImageNamed:@"CelesDeath1.gif"];
        playerHit.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesDeath1 = [SKTexture textureWithImageNamed:@"CelesDeath2.gif"];
        CelesDeath1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesDeath2 = [SKTexture textureWithImageNamed:@"CelesDeath3.gif"];
        CelesDeath2.filteringMode = SKTextureFilteringNearest;
        
        playerDeath= [SKAction animateWithTextures:@[playerHit, CelesDeath1,CelesDeath2] timePerFrame:0.5];
        
        //Laugh
        SKTexture* CelesLaugh1 = [SKTexture textureWithImageNamed:@"CelesLaugh1.tiff"];
        CelesLaugh1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesLaugh2 = [SKTexture textureWithImageNamed:@"CelesLaugh2.tiff"];
        CelesLaugh2.filteringMode = SKTextureFilteringNearest;
        
        playerLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesLaugh1, CelesLaugh2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* CelesTalk1 = [SKTexture textureWithImageNamed:@"CelesTalk1.tiff"];
        CelesTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesTalk2 = [SKTexture textureWithImageNamed:@"CelesTalk2.tiff"];
        CelesTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesTalk1, CelesTalk2] timePerFrame:0.2]];
        
        //Celes check
        
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }
}

-(void) makePrincessAnimation{
    
    
    //Walk action Right
    SKTexture* MariaWalkLeft1 = [SKTexture textureWithImageNamed:@"MariaWalkLeft1.tiff"];
    MariaWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaWalkLeft2 = [SKTexture textureWithImageNamed:@"MariaWalkLeft2.tiff"];
    MariaWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaWalkLeft3 = [SKTexture textureWithImageNamed:@"MariaWalkLeft3.tiff"];
    MariaWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaWalkLeft4 = [SKTexture textureWithImageNamed:@"MariaWalkLeft4.tiff"];
    MariaWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    mariaWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[MariaWalkLeft1,MariaWalkLeft2,MariaWalkLeft3,MariaWalkLeft4] timePerFrame:0.2]];
    
    mariaLeft = [SKTexture textureWithImageNamed:@"MariaStandLeft.gif"];
    
    mariaRight = [SKTexture textureWithImageNamed:@"MariaStandRight.gif"];
    
    
    
    
    //Sing
    SKTexture* MariaSing1 = [SKTexture textureWithImageNamed:@"MariaSing1.tiff"];
    MariaSing1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaSing2 = [SKTexture textureWithImageNamed:@"MariaSing2.tiff"];
    MariaSing2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaSing3 = [SKTexture textureWithImageNamed:@"MariaSing3.tiff"];
    MariaSing3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaSing4 = [SKTexture textureWithImageNamed:@"MariaSing4.tiff"];
    MariaSing4.filteringMode = SKTextureFilteringNearest;
    
    mariaSing = [SKAction repeatActionForever:[SKAction animateWithTextures:@[MariaSing1, MariaSing2,MariaSing3, MariaSing4] timePerFrame:0.4]];
    
}

-(void) makeEnemyAnimation{
    //Harpie Animations
    for(int i = 1;i < 18; i++){
        NSString *flyLeftStr = [NSString stringWithFormat:@"harpieFlyLeft-%d.png", i];
        NSString *flyRightStr = [NSString stringWithFormat:@"harpieFlyRight-%d.png", i];
        
        
        harpieFlyLeft[i] = [SKTexture textureWithImageNamed: flyLeftStr];
        harpieFlyRight[i] = [SKTexture textureWithImageNamed: flyRightStr];
        harpieFlyLeft[i].filteringMode = SKTextureFilteringNearest;
        harpieFlyRight[i].filteringMode = SKTextureFilteringNearest;
        
        
    }
    harpieFlyL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieFlyLeft[1],harpieFlyLeft[2],harpieFlyLeft[3],harpieFlyLeft[4],harpieFlyLeft[5],harpieFlyLeft[6],harpieFlyLeft[7],harpieFlyLeft[8],harpieFlyLeft[9],harpieFlyLeft[10],harpieFlyLeft[11],harpieFlyLeft[12],harpieFlyLeft[13],harpieFlyLeft[14],harpieFlyLeft[15],harpieFlyLeft[16],harpieFlyLeft[17]] timePerFrame:0.2 resize:false restore:false]];
    harpieFlyR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieFlyRight[1],harpieFlyRight[2],harpieFlyRight[3],harpieFlyRight[4],harpieFlyRight[5],harpieFlyRight[6],harpieFlyRight[7],harpieFlyRight[8],harpieFlyRight[9],harpieFlyRight[10],harpieFlyRight[11],harpieFlyRight[12],harpieFlyRight[13],harpieFlyRight[14],harpieFlyRight[15],harpieFlyRight[16],harpieFlyRight[17]] timePerFrame:0.2 resize:false restore:false]];
    
    for(int i = 1;i < 9; i++){
        NSString *deathLeftStr = [NSString stringWithFormat:@"harpieDeathLeft-%d.png", i];
        NSString *deathRightStr = [NSString stringWithFormat:@"harpieDeathRight-%d.png", i];
        
        
        harpieDeathLeft[i] = [SKTexture textureWithImageNamed: deathLeftStr];
        harpieDeathRight[i] = [SKTexture textureWithImageNamed: deathRightStr];
        harpieDeathLeft[i].filteringMode = SKTextureFilteringNearest;
        harpieDeathRight[i].filteringMode = SKTextureFilteringNearest;
        
        
        
    }
    harpieDeathL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieDeathLeft[1],harpieDeathLeft[2],harpieDeathLeft[3],harpieDeathLeft[4],harpieDeathLeft[5],harpieDeathLeft[6],harpieDeathLeft[7],harpieDeathLeft[8]] timePerFrame:0.2 resize:false restore:false]];
    
    harpieDeathR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieDeathRight[1],harpieDeathRight[2],harpieDeathRight[3],harpieDeathRight[4],harpieDeathRight[5],harpieDeathRight[6],harpieDeathRight[7],harpieDeathRight[8]] timePerFrame:0.2 resize:false restore:false]];
    
    
    for(int i = 1;i < 11; i++){
        NSString *attackLeftStr = [NSString stringWithFormat:@"harpieAttackLeft-%d.png", i];
        NSString *attackRightStr = [NSString stringWithFormat:@"harpieAttackRight-%d.png", i];
        
        
        harpieAttackLeft[i] = [SKTexture textureWithImageNamed: attackLeftStr];
        harpieAttackRight[i] = [SKTexture textureWithImageNamed: attackRightStr];
        
        harpieAttackLeft[i].filteringMode = SKTextureFilteringNearest;
        
        harpieAttackRight[i].filteringMode = SKTextureFilteringNearest;
        
        
    }
    
    harpieAttackL = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieAttackLeft[1],harpieAttackLeft[2],harpieAttackLeft[3],harpieAttackLeft[4],harpieAttackLeft[5],harpieAttackLeft[6],harpieAttackLeft[7],harpieAttackLeft[8],harpieAttackLeft[9],harpieAttackLeft[10]] timePerFrame:0.2 resize:false restore:false]];
    
    harpieAttackR = [SKAction repeatActionForever:[SKAction animateWithTextures:@[harpieAttackRight[1],harpieAttackRight[2],harpieAttackRight[3],harpieAttackRight[4],harpieAttackRight[5],harpieAttackRight[6],harpieAttackRight[7],harpieAttackRight[8],harpieAttackRight[9],harpieAttackRight[10]] timePerFrame:0.2 resize:false restore:false]];
    

}


-(void) makeBossAnimation{
    
    //Boss
    
    bossLeft = [SKTexture textureWithImageNamed:@"BossLeft.gif"];
    bossTiltLeftUp = [SKTexture textureWithImageNamed:@"BossLeftTiltUp.gif"];
    bossTiltLeftDown = [SKTexture textureWithImageNamed:@"BossLeftTiltDown.gif"];
    bossRight = [SKTexture textureWithImageNamed:@"BossRight.gif"];
    bossTiltRightUp = [SKTexture textureWithImageNamed:@"BossRightTiltUp.gif"];
    bossTiltLeftDown = [SKTexture textureWithImageNamed:@"BossRightTiltDown.gif"];
    
}


-(void)makeFireBall{
    [fireBall removeFromParent];
    fireBall = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
    //Fireball to the left or right of the player
    if(playerFacingLeft == false){
        [fireBall setPosition: CGPointMake(player.position.x + 30,player.position.y +10)];
    }
    else{
        [fireBall setPosition: CGPointMake(player.position.x - 30,player.position.y + 10)];
    }
    fireBall.size = CGSizeMake(60, 60);
    
    fireBall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fireBall.texture.size];
    
    fireBall.physicsBody.affectedByGravity = false;
    fireBall.physicsBody.allowsRotation = false;
    fireBall.physicsBody.categoryBitMask = fireBallCategory;
    fireBall.physicsBody.contactTestBitMask = enemyCategory;
    fireBall.physicsBody.usesPreciseCollisionDetection = YES;
    [self addChild: fireBall];
}

-(void) makeBossFire {
    [self clearBossFire];
    
    for (int i = 0; i <3; ++i) {
        bossFire[i] = [SKSpriteNode spriteNodeWithImageNamed:@"Firework3.gif"];
        bossFire[i].size = CGSizeMake(80.0, 80.0);
        bossFire[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bossFire[i].texture.size];
        bossFire[i].physicsBody.affectedByGravity = false;
        bossFire[i].physicsBody.allowsRotation = false;
        bossFire[i].physicsBody.categoryBitMask = enemyProjCategory;
        bossFire[i].physicsBody.contactTestBitMask = playerCategory|groundCategory;
        bossFire[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        NSString *str = @"bossFire";
        NSString* fireNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:fireNum];
        
        bossFire[i].name = str;
    }
}

-(void) clearBossFire {
    for(int i = 0; i < 3; i++){
        [bossFire[i] removeFromParent];
    }
}

-(void) pattern1 {
    [self makeBossFire];
    for (int i = 0; i < 3; ++i) {
        [bossFire[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
        [self addChild:bossFire[i]];
        
    }
    
    [bossFire[0].physicsBody applyImpulse:CGVectorMake(-10, 15)];
    [bossFire[1].physicsBody applyImpulse:CGVectorMake(-10, -15)];
    [bossFire[2].physicsBody applyImpulse:CGVectorMake(-10, 0)];

}


-(void) pattern2 {
    [self makeBossFire];
    for (int i = 0; i < 3; ++i) {
        [bossFire[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
        [self addChild:bossFire[i]];
        
    }
    
    [bossFire[0].physicsBody applyImpulse:CGVectorMake(-10, -5)];
    [bossFire[1].physicsBody applyImpulse:CGVectorMake(-10, -10)];
    [bossFire[2].physicsBody applyImpulse:CGVectorMake(-10, -15)];
    
}

-(void) pattern3 {
    [self makeBossFire];
    for (int i = 0; i < 3; ++i) {
        [bossFire[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
        [self addChild:bossFire[i]];
        
    }
    
    [bossFire[0].physicsBody applyImpulse:CGVectorMake(-10, 15)];
    [bossFire[1].physicsBody applyImpulse:CGVectorMake(-10, 10)];
    [bossFire[2].physicsBody applyImpulse:CGVectorMake(-10, 5)];
    
}

-(void) bossDirectAttack:(int) i{
    [self clearBossFire];
    int playerX = player.position.x;
    int playerY = player.position.y;
    SKAction *goToPlayer;
    
    bossFire[i] = [SKSpriteNode spriteNodeWithImageNamed:@"Firework3.gif"];
    bossFire[i].size = CGSizeMake(80.0, 80.0);
    [bossFire[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
    bossFire[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bossFire[i].texture.size];
    bossFire[i].physicsBody.affectedByGravity = false;
    bossFire[i].physicsBody.allowsRotation = false;
    bossFire[i].physicsBody.categoryBitMask = enemyProjCategory;
    bossFire[i].physicsBody.contactTestBitMask = playerCategory|groundCategory;
    bossFire[i].physicsBody.usesPreciseCollisionDetection = YES;
    
    NSString *str = @"bossFire";
    NSString* fireNum = [NSString stringWithFormat:@"%i", i];
    str = [str stringByAppendingString:fireNum];
    
    bossFire[i].name = str;
    [self addChild:bossFire[i]];

    
    
    goToPlayer = [SKAction moveTo: CGPointMake(playerX, playerY) duration: 2];
    [bossFire[i] runAction:goToPlayer];
    
}


//fire button
- (void)makeFireButton
{
    fireButton = [SKSpriteNode spriteNodeWithImageNamed:@"fireButton.png"];
    fireButton.size = CGSizeMake(100 ,100);
    fireButton.position = CGPointMake(player.position.x,-620);
    fireButton.name = @"fireButtonNode";//how the node is identified later
    
    [self addChild: (fireButton)];
    
}


-(void) makeHealthLabel{
    healthLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    healthLabel.position = CGPointMake(player.position.x+300, 600);
    healthLabel.fontSize = 50;
    healthLabel.fontColor = [SKColor redColor];
    healthLabel.name = @"healthLabel";
    
    NSString *hpLeft = [NSString stringWithFormat:@"HP : %d",hitPoints];
    healthLabel.text = hpLeft;
    
    [self addChild:healthLabel];
    
}


-(void) makeDefeatLabel{
    defeatLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    defeatLabel.position = CGPointMake(player.position.x, player.position.y+400);
    defeatLabel.fontSize = 40;
    defeatLabel.fontColor = [SKColor redColor];
    defeatLabel.name = @"defeatLabel";
    
    NSString * gameOverText = [NSString stringWithFormat:@"You failed to save the princess!"];
    defeatLabel.text = gameOverText;
    
    [self addChild:defeatLabel];
    
    
}

-(void) makeVictoryLabel{
    victoryLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    victoryLabel.position = CGPointMake(player.position.x, player.position.y+200);
    victoryLabel.fontSize = 30;
    victoryLabel.fontColor = [SKColor greenColor];
    victoryLabel.name = @"victoryLabel";
    
    NSString *victoryLeft = [NSString stringWithFormat:@"You saved the princess and got a date."];
    victoryLabel.text = victoryLeft;
    
    [self addChild:victoryLabel];
    
    
}


-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    
    if ([node.name isEqualToString:@"fireButtonNode"] && hitPoints > 0) {
        [self makeFireBall];
        if(playerFacingLeft == true){
            [fireBall.physicsBody applyImpulse:CGVectorMake(-15, 0)];
        }
        else{
            [fireBall.physicsBody applyImpulse:CGVectorMake(15, 0)];
        }
    }
    
    else if ([node.name isEqualToString:@"reset"]) {
        //[self reset];
    }
    
    else{
        
        if(hitPoints > 0){
            if(location.y <= player.position.y){
                if(location.x > player.position.x && location.y){
                    [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
                    if(running == false || playerFacingLeft == true){
                        [player runAction:playerWalkRight];
                    }
                    running = true;
                    playerFacingLeft = false;
                    
                }
                else{
                    [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                    if(running == false || playerFacingLeft == false){
                        [player runAction:playerWalkLeft];
                    }
                    running = true;
                    
                    playerFacingLeft = true;
                    
                }
            }
            else{
                //Enter cheat here if take too long
                if(player.physicsBody.velocity.dy == 0){
                    if(location.x > player.position.x && location.y){
                        playerFacingLeft = false;
                        
                        [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
                        [player runAction:playerJumpRight];
                        running = false;
                        
                    }
                    else{
                        playerFacingLeft = true;
                        [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                        [player runAction:playerJumpLeft];
                        running = false;
                        
                    }
                }
                //Cheat here
            }
            
        }
        
    }
    
}

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask == enemyCategory)&&
        (secondBody.categoryBitMask == enemyCategory))
    {
        //Figure out how to unclog imps
    }
    
    if ((firstBody.categoryBitMask == enemyCategory)&& (secondBody.categoryBitMask == fireBallCategory))
    {
        // printf("Fireball hit");
        //NSLog(@"%@ \n" , firstBody.node.name);
        //How know what object to remove
        if([firstBody.node.name  isEqualToString: @"dragonBoss"]){
            bossHealth--;
            printf("%d", bossHealth);
        }
        if([firstBody.node.name  isEqualToString: @"harpie0"]){
            harpieHP[0]--;
            if(harpieHP[0] ==0){
                [harpies[0] removeAllActions];
                [harpies[0] runAction:harpieDeathL];
                [harpies[0] removeFromParent];
                harpiesAlive[0] =false;
            }
        }
        if([firstBody.node.name  isEqualToString: @"harpie1"]){
            harpieHP[1]--;
            if(harpieHP[1] ==0){
                [harpies[1] removeAllActions];
                [harpies[1] runAction:harpieDeathL];
                [harpies[1] removeFromParent];
                harpiesAlive[1] =false;
            }
            
        }
        if([firstBody.node.name  isEqualToString: @"harpie2"]){
            harpieHP[2]--;
            if(harpieHP[2] ==0){
                [harpies[2] removeAllActions];
                [harpies[2] runAction:harpieDeathL];
                [harpies[2] removeFromParent];
                harpiesAlive[2] =false;
            }
            
        }
        
        if([firstBody.node.name  isEqualToString: @"harpie3"]){
            harpieHP[3]--;
            if(harpieHP[3] ==0){
                [harpies[3] removeAllActions];
                [harpies[3] runAction:harpieDeathL];
                [harpies[3] removeFromParent];
                harpiesAlive[3] =false;
            }
            
        }
        [fireBall removeFromParent];
        
        
    }
    
    
    if ((firstBody.categoryBitMask == enemyCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        hitPoints --;
        
        if (hitPoints > 0){
            [player removeAllActions];
            [player setTexture: playerHit];
            if([firstBody.node.name  isEqualToString: @"dragonBoss"]){
                if(playerFacingLeft == true){
                    [player.physicsBody applyImpulse:CGVectorMake(15, 30)];
                }else{
                    [player.physicsBody applyImpulse:CGVectorMake(-15, 30)];
                }
            }
        }
        
    }
    if ((firstBody.categoryBitMask == enemyProjCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        hitPoints --;

        if (hitPoints > 0){
            [player removeAllActions];
            [player setTexture: playerHit];
        }
        
        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFire[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFire[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFire[2] removeFromParent];
        }
        
        
        
    }
    
    //If fireballs hits ground it should delete
    if ((firstBody.categoryBitMask == enemyProjCategory)&&
        (secondBody.categoryBitMask == groundCategory))
    {
        
        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFire[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFire[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFire[2] removeFromParent];
        }

        
        
    }
    if ((firstBody.categoryBitMask == enemyProjCategory)&&
        (secondBody.categoryBitMask == platformCategory))
    {
        
        
        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFire[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFire[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFire[2] removeFromParent];
        }
        
        
        
    }

    
    
    [[SharedData sharedInstance] setPlayerHealth:hitPoints];
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    hitPoints = [[SharedData sharedInstance] playerHealth];
    
    if(hitPoints >= 0){
        NSString *hpLeft = [NSString stringWithFormat:@"HP : %d",hitPoints];
        healthLabel.text = hpLeft;
    }
    
    if(player.position.y > 0 && player.position.y < 3500){
        //cam.position = CGPointMake(CGRectGetMidX(self.frame), player.position.y);
        //fireButton.position = CGPointMake(0, player.position.y - 500);
        resetButton.position = CGPointMake(player.position.x + 270, -620);
        
    }
    
    if(bossHealth == -2 && player.position.y > 300){
        cam.position = CGPointMake(CGRectGetMidX(self.frame), player.position.y - 300);
    }
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    //printf("Player x : %f y : %f \n",player.position.x,player.position.y);
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    if(player.physicsBody.velocity.dx == 0 && player.physicsBody.velocity.dy == 0 && hitPoints > 0){
        [player removeAllActions];
        if(player.position.x < 20 && player.position.x > -20){
            [player setTexture: playerIdle];
            running = false;
        }
        else{
            if(playerFacingLeft == true){
                running = false;
                [player setTexture: playerLeft];
            }else{
                running = false;
                [player setTexture:playerRight];
            }
        }
    }
    
    if(bossHealth <= 10 && harpiesSpawn == false){
        [self clearBossFire];
        [self makeHarpies];
        harpiesSpawn = true;
    }
    
    if(bossHealth <= 10 && harpiesSpawn == true){
        minionCheck = false;
        for(int i = 0; i < 4; i++){
            if(harpiesAlive[i] == true){
                minionCheck = true;
            }
        }
    }
    
    if(bossHealth <= 10 && harpiesSpawn == true && minionCheck == true){
        [dragonBoss runAction:[SKAction moveTo:CGPointMake(300,400) duration: 4]];
        [dragonBoss setTexture:bossTiltLeftDown];
    }
    else if(bossHealth <= 10 && minionCheck == false && harpiesSpawn == true){
        [dragonBoss runAction:[SKAction moveTo:CGPointMake(300,0) duration: 4]];
        [dragonBoss setTexture:bossLeft];
    }
    
    if(harpiesSpawn == true && bossHealth > 0 && bossHealth < 11){
        if(frameCounter == 0 && bossHealth <= 10){
            [self bossDirectAttack:0];
        }
        if(frameCounter == 30*2 && bossHealth <= 10){
            [self bossDirectAttack:1];
        }
        if(frameCounter == 30*4 && bossHealth <= 10){
            frameCounter = 0;
        }
        
    }
    
    //Enenmy movements here
    //Harpie movement
    for(int i = 0; i < 4; i++){
        if(harpies[i].physicsBody.velocity.dx == 0 && harpieDirectLeft[i] == true){
            //[harpies[i] runAction:[SKAction moveTo:CGPointMake(-300,harpies[i].position.y) duration: 5]];
            [harpies[i].physicsBody applyImpulse:CGVectorMake(-50, 0)];
            //if(harpieFlying[i] == false){
            [harpies[i] runAction:harpieFlyL];
            harpieFlying[i] = true;
            harpieAttacking[i] = false;
            
            //}
            //harpieDirectLeft[i] = false;
            
        }
        else if(harpies[i].physicsBody.velocity.dx == 0 && harpieDirectLeft[i] == false){
            //[harpies[i] runAction:[SKAction moveTo:CGPointMake(300,harpies[i].position.y) duration: 5]];
            [harpies[i].physicsBody applyImpulse:CGVectorMake(50, 0)];
            //if(harpieFlying[i] == false){
            [harpies[i] runAction:harpieFlyR];
            harpieFlying[i] = true;
            harpieAttacking[i] = false;
            
            //}
            
            //harpieDirectLeft[i] = true;
            
        }
        
        if(harpies[i].position.x <= -290){
            harpieDirectLeft[i] = false;
        }
        if(harpies[i].position.x >= 290){
            harpieDirectLeft[i] = true;
        }
        
        if(harpieDirectLeft[i] == true){
            if(player.position.x > harpies[i].position.x - 150 && player.position.y > harpies[i].position.y - 200){
                if(harpieAttacking[i] == false){
                    [harpies[i] runAction:harpieAttackL];
                }
                
                harpieAttacking[i] = true;
                harpieFlying[i] =false;
                
            }else{
                if(harpieFlying[i] == false){
                    [harpies[i] runAction:harpieFlyL];
                    harpieFlying[i] =true;
                }
                harpieAttacking[i] = false;
                
                
            }
        }
        if(harpieDirectLeft[i] == false){
            if(player.position.x < harpies[i].position.x + 300 && player.position.y > harpies[i].position.y - 200){
                if(harpieAttacking[i] == false){
                    [harpies[i] runAction:harpieAttackR];
                }
                
                harpieAttacking[i] = true;
                harpieFlying[i] =false;
                
            }else{
                if(harpieFlying[i] == false){
                    [harpies[i] runAction:harpieFlyR];
                    harpieFlying[i] =true;
                }
                harpieAttacking[i] =false;
                
            }
        }
        if(harpies[i].position.y > 200){
            [harpies[i].physicsBody applyImpulse:CGVectorMake(0, -25)];

        }
        
        
        
    }
    
    
    for(int i = 0; i < 2; i++){
        //printf("Frame count %d \n," ,frameCounter);
        if(frameCounter == 0 || frameCounter == 30*4 || frameCounter == 30*8){
            if(platUp[i] == true){
                printf("Plat up \n");

                if(i == 0){
                    [platform[i] runAction:[SKAction moveTo:CGPointMake(-300,200) duration: 4]];
                }else{
                    [platform[i] runAction:[SKAction moveTo:CGPointMake(300,200) duration: 4]];
                }
                platUp[i] = false;
            }
            else{
                printf("Plat down \n");

                if(i == 0){
                    [platform[i] runAction:[SKAction moveTo:CGPointMake(-300,-400) duration: 4]];
                }else{
                    [platform[i] runAction:[SKAction moveTo:CGPointMake(300,-400) duration: 4]];
                }
                platUp[i] = true;

            }
        }
    }
    
    
    if (frameCounter ==30 * 2 && bossHealth > 10) {
        int patternNum = arc4random_uniform(3);
        printf(" Pattern %d", patternNum);
        if(patternNum == 0){
            [self pattern1];
        }
        if(patternNum == 1){
            [self pattern2];
        }
        if(patternNum == 2){
            [self pattern3];
        }
        
    }
    if(bossHealth > 10 && frameCounter == 30*4){
        frameCounter = 0;
    }

    if(bossHealth == 0){
        [dragonBoss removeFromParent];
        [self clearBossFire];
        for(int i = 0;i < 2;i++){
            [platform[i] removeFromParent];
        }
        [fireBall removeFromParent];

        [self platToPrincess];
        bossHealth = -1;
    }
    
    if(bossHealth == -1 && player.position.x < platToPrincess.position.x +50 && player.position.y > platToPrincess.position.y + 20){
        [platToPrincess runAction:[SKAction moveTo:CGPointMake(platToPrincess.position.x,1400) duration: 10]];
        [princess runAction: mariaSing];
        [healthLabel removeFromParent];
        [fireButton removeFromParent];
        [resetButton removeFromParent];

        bossHealth = -2;
        
    }
    
    
    if(hitPoints == 0){
        [player removeAllActions];
        [healthLabel removeFromParent];
        [player runAction:playerDeath];
        hitPoints = -10;
    }
    
    
    
    if(player.position.y > princessPlat.position.y-30 && player.position.x > princessPlat.position.x-50 && hitPoints > 0){
        [player setPosition: CGPointMake(-100,1370)];
        [player removeAllActions];
        printf("Victory");
        hitPoints = -1;
        [[SharedData sharedInstance] setPlayerHealth:hitPoints];
        [princess removeAllActions];
        [princess runAction:mariaWalkLeft];
        [princess runAction:[SKAction moveTo:CGPointMake(player.position.x+50,player.position.y) duration: 2]];
        
    }
    
    if(princess.position.x -50 < player.position.x && hitPoints == -1){
        [princess removeAllActions];
        [princess setTexture:mariaLeft];
        [player runAction:playerVictory];
        hitPoints = -2;
        [[SharedData sharedInstance] setPlayerHealth:hitPoints];
        [self makeVictoryLabel];
    }
    
    
    _lastUpdateTime = currentTime;
    frameCounter++;
}
@end
