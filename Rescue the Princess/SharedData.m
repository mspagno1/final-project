//
//  SharedData.m
//  Rescue the Princess
//
//  Created by Vaster on 5/10/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharedData.h"

@implementation SharedData
@synthesize characterNumber;
@synthesize playerHealth;

static SharedData * singleton = nil;

-(id)init
{
    if (singleton)
        return singleton;
    
    self = [super init];
    if (self)
    {
        singleton = self;
    }
    
    return self;
}

+(SharedData *)sharedInstance
{
    if (singleton)
        return singleton;
    
    return [SharedData init];
}

@end
