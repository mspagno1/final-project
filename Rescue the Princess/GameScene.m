//
//  GameScene.m
//  Rescue the Princess
//
//  Created by Vaster on 5/9/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameScene.h"

//Masks of the two coliding objects

static const uint32_t enemyProjCategory = 0x1 << 0;
static const uint32_t enemyCategory =  0x1 << 1;
static const uint32_t playerCategory =  0x1 << 2;
static const uint32_t groundCategory =  0x1 << 3;
static const uint32_t fireBallCategory = 0x1 << 4;
static const uint32_t backGroundCategory = 0x1 << 5;




//static const uint32_t backgroundCategory =  0x1 << 3;

@implementation GameScene {
    NSTimeInterval _lastUpdateTime;
    
    //Scenary and level design
    SKSpriteNode * background[4];
    SKSpriteNode * towerWall[3];
    
    SKSpriteNode * ground[3];
    SKSpriteNode * groundPhase2[5];
    SKSpriteNode * groundPhase3[5];
    
    
    SKSpriteNode * fireButton;
    SKSpriteNode * resetButton;
    SKSpriteNode * backBorder;
    SKSpriteNode * frontBorder;

    //Player
    SKSpriteNode * player;
    //Enemies
    
    SKSpriteNode * imp[6];
    
    SKSpriteNode * mage[2];
    
    Boolean mageAlive[2];
    
    //Attacks
    SKSpriteNode * fireBall;
    SKSpriteNode * mageFireBall[2];
    
    
    //Player actions
    SKAction* playerJumpLeft;
    SKAction* playerWalkLeft;
    SKAction* playerJumpRight;
    SKAction* playerWalkRight;
    SKAction* playerDeath;
    SKAction* playerLaugh;
    SKAction* playerVictory;
    SKAction* playerTalk;
    SKAction* playerCheck;

    
    SKTexture* playerHit;
    SKTexture* playerRight;
    SKTexture* playerLeft;
    SKTexture* playerIdle;
    
    
    //Imp Actions
    SKAction * impRight;
    SKAction * impLeft;
    
    Boolean impDirectionLeft[6];

    
    //True is player left false is player is right
    Boolean playerFacingLeft;
    Boolean running;
    Boolean endOfLevel;
    Boolean secondPhaseSpawn;
    Boolean thirdPhaseSpawn;
    
    SKCameraNode * cam;
    
    int checkPointX;
    int checkPointY;
    int hitPoints;
    
    //Labels
    SKLabelNode * defeatLabel;
    SKLabelNode * healthLabel;
    
    //Timeframe counter
    int frameCounter;
    
}

- (void)sceneDidLoad {
    static BOOL didLoad = NO;
    
    endOfLevel = false;
    frameCounter = 0;
    secondPhaseSpawn = false;
    thirdPhaseSpawn = false;
    running = false;
    
    //Scene is being loaded twice for some reason, camera only works on second load
    //LOOKS INTO THIS AT SOME POINT BUT THIS IS A TEMP FIX
    if (didLoad)
    {
        cam = [SKCameraNode node];
        self.camera = cam;
        //NSLog(@"Skipping the second load");
        return;
    }
    didLoad = YES;
    
    
    //Make background
    
    [self makeBackground];
    [self makeTower];

    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );
    self.physicsWorld.contactDelegate = self;
    
    //Make level
    [self levelSetUp];
    
    
    //Make player
    [self makePlayer];
    [self makeAnimation];
    
    //Make init bad guys
    [self makeImpsPhase1];
    [self makeEnemyAnimation];
    [self makeMage];

    
    //MakeUI
    [self makeFireButton];
    [self makeResetButton];
    
    //Make labels
    [self makeHealthLabel];
    
    //Make Borders
    [self makeBorders];
    
    //Checkpoint initial
    checkPointX = player.position.x;
    checkPointY = player.position.y +10;
    
    //Players Health
    hitPoints =  [[SharedData sharedInstance] playerHealth];
    if(hitPoints == 0){
        [[SharedData sharedInstance] setPlayerHealth:20];

    }
    //[[SharedData sharedInstance] setPlayerHealth:20];
    

}

-(void) makeBackground{
    for(int i = 0; i < 4; i++){
        if((float)((int)i % (int)2) == 0){
            background[i] = [SKSpriteNode spriteNodeWithImageNamed: @"background.png"];
        }else{
            background[i] = [SKSpriteNode spriteNodeWithImageNamed: @"background2.png"];
        }
        background[i].size = CGSizeMake(3000,self.frame.size.height);
    
        //[background[i] setPosition: CGPointMake(0 + 1900*i,0)];
        [background[i] setPosition: CGPointMake(0 + 3000*i,0)];

        background[i].physicsBody.affectedByGravity = FALSE;
        background[i].physicsBody.categoryBitMask = backGroundCategory;
        [self addChild: (background[i])];
    }
    
}

-(void)makeBorders{
    backBorder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(20, 4000)];
    
    [backBorder setPosition: CGPointMake(player.position.x - 100,player.position.y)];
    backBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(30, 4000)];
    backBorder.physicsBody.dynamic = NO;

    backBorder.physicsBody.affectedByGravity = FALSE;
    backBorder.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (backBorder)];
    

}

-(void) makeGround: (int)xLoc : (int)yLoc :(int)xValSize : (int)elem{
    ground[elem]  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    ground[elem].size = CGSizeMake(xValSize, ground[elem].texture.size.height + 500);
    
    [ground[elem] setPosition: CGPointMake(xLoc,yLoc)];
    
    ground[elem].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(xValSize, 10)];
    ground[elem].physicsBody.dynamic = NO;
    ground[elem].physicsBody.affectedByGravity = FALSE;
    ground[elem].physicsBody.categoryBitMask = groundCategory;
    [self addChild: (ground[elem])];
}

-(void) makeGroundPhase2{
    for(int i = 0; i < 5; i++){
        groundPhase2[i]  = [SKSpriteNode spriteNodeWithImageNamed: @"smallBlock.png"];
        groundPhase2[i].size = CGSizeMake(200, 500 +i* 125);
        
        [groundPhase2[i] setPosition: CGPointMake(1800 + i * 500,-518)];
        
        groundPhase2[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(groundPhase2[i].size.width, 280 + i*40)];
        //groundPhase2[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:groundPhase2[i].size];

        groundPhase2[i].physicsBody.dynamic = NO;
        groundPhase2[i].physicsBody.affectedByGravity = FALSE;
        groundPhase2[i].physicsBody.categoryBitMask = groundCategory;
        [self addChild: (groundPhase2[i])];
    }
}

-(void) makeGroundPhase3{
    for(int i = 0; i < 5; i++){
        groundPhase3[i]  = [SKSpriteNode spriteNodeWithImageNamed: @"smallBlock.png"];
        groundPhase3[i].size = CGSizeMake(200, 500 +i* 125);
        
        [groundPhase3[i] setPosition: CGPointMake(6600 + i * 500,-518)];
        
        groundPhase3[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(groundPhase3[i].size.width, 280 + i*40)];
        //groundPhase2[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:groundPhase2[i].size];
        
        groundPhase3[i].physicsBody.dynamic = NO;
        groundPhase3[i].physicsBody.affectedByGravity = FALSE;
        groundPhase3[i].physicsBody.categoryBitMask = groundCategory;
        [self addChild: (groundPhase3[i])];
    }
}

-(void)makeTower{
    for(int i = 0; i < 3; i++){
        
        towerWall[i] = [SKSpriteNode spriteNodeWithImageNamed: @"Wall.png"];
    
        towerWall[i].size = CGSizeMake(2000,self.frame.size.height);
        
        [towerWall[i] setPosition: CGPointMake(10500+1000*i,0)];
        
        towerWall[i].physicsBody.affectedByGravity = FALSE;
        towerWall[i].physicsBody.categoryBitMask = backGroundCategory;
        [self addChild: (towerWall[i])];
    }
}

-(void)levelSetUp{
    [self makeGround:0:-518:3000:0];
    [self makeGroundPhase2];
    [self makeGround:5500:-518:2000:1];
    [self makeGroundPhase3];
    [self makeGround:10200:-518:3000:2];


}

-(void)cleanUp:(int)maxLoc{
    for(int i = 0; i < 3; i++){
        if(ground[i].position.x < maxLoc){
            [ground[i] removeFromParent];
        }
    }
    for(int i = 0; i < 5; i++){
        if(groundPhase2[i].position.x < maxLoc){
            [groundPhase2[i] removeFromParent];
        }
    }
    for(int i = 0; i < 5; i++){
        if(groundPhase3[i].position.x < maxLoc){
            [groundPhase3[i] removeFromParent];
        }
    }
    for(int i = 0; i < 4; i++){
        if(background[i].position.x < maxLoc - 2000){
            [background[i] removeFromParent];
        }
    }
    for(int i = 0; i < 6; i++){
        if(imp[i].position.x < maxLoc){
            [imp[i] removeFromParent];
        }
    }
}

-(void)reset{
    for(int i = 0; i < 5; i++){
        [groundPhase2[i] removeFromParent];
        [groundPhase3[i] removeFromParent];
    }
    for(int i = 0; i <3 ; i++){
        [ground[i] removeFromParent];
    }
    for(int i = 0; i < 4; i++){
        [background[i] removeFromParent];
    }
    for(int i = 0; i < 6; i++){
        [imp[i] removeFromParent];
    }
    for(int i = 0; i < 2; i++){
        [mage[i] removeFromParent];
        [mageFireBall[i] removeFromParent];
    }
    for(int i = 0; i < 3; i++){
        [towerWall[i] removeFromParent];
    }
    [player removeFromParent];
    [fireBall removeFromParent];
    [backBorder removeFromParent];
    [defeatLabel removeFromParent];
    [healthLabel removeFromParent];
    [resetButton removeFromParent];
    [fireButton removeFromParent] ;
    
    //Restart lvl
    hitPoints = 20;
    [[SharedData sharedInstance] setPlayerHealth:hitPoints];
    
    [self makeBackground];
    [self makeTower];
    
    //Make level
    [self levelSetUp];
    
    
    //Make player
    [self makePlayer];
    [self makeAnimation];
    
    //Make init bad guys
    [self makeImpsPhase1];
    [self makeEnemyAnimation];
    [self makeMage];
    
    //MakeUI
    [self makeFireButton];
    [self makeResetButton];
    
    //Make labels
    [self makeHealthLabel];
    
    //Make Borders
    [self makeBorders];

    
}

-(void) makePlayer{
    //Set up player
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        playerIdle = [SKTexture textureWithImageNamed:@"Terra.gif"];
    }else{
        playerIdle = [SKTexture textureWithImageNamed:@"Celes.gif"];
    }
    player = [SKSpriteNode spriteNodeWithTexture: playerIdle];
    [player setPosition: CGPointMake(-1000,-517)];
    player.size = CGSizeMake(80, 100);
    
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    player.physicsBody.categoryBitMask = playerCategory;
    player.physicsBody.contactTestBitMask = enemyCategory;
    player.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self addChild: (player)];
}



-(void) makeAnimation{
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        //Walk action Right
        SKTexture* TerraWalkRight1 = [SKTexture textureWithImageNamed:@"TerraWalkRight1.gif"];
        TerraWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight2 = [SKTexture textureWithImageNamed:@"TerraWalkRight2.gif"];
        TerraWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight3 = [SKTexture textureWithImageNamed:@"TerraWalkRight3.gif"];
        TerraWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkRight4 = [SKTexture textureWithImageNamed:@"TerraWalkRight4.gif"];
        TerraWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkRight1,  TerraWalkRight2,TerraWalkRight3,TerraWalkRight4] timePerFrame:0.2]];
        
        SKTexture* TerraWalkLeft1 = [SKTexture textureWithImageNamed:@"TerraWalkLeft1.gif"];
        TerraWalkLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft2 = [SKTexture textureWithImageNamed:@"TerraWalkLeft2.gif"];
        TerraWalkLeft2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft3 = [SKTexture textureWithImageNamed:@"TerraWalkLeft3.gif"];
        TerraWalkLeft3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWalkLeft4 = [SKTexture textureWithImageNamed:@"TerraWalkLeft4.gif"];
        TerraWalkLeft4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkLeft1,  TerraWalkLeft2,TerraWalkLeft3,TerraWalkLeft4] timePerFrame:0.2]];
        
        playerRight = TerraWalkRight1;
        
        playerLeft = TerraWalkLeft1;
        
        //Jump action Left
        SKTexture* TerraJumpLeft1 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft.gif"];
        TerraJumpLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraJumpLeft2 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft2.gif"];
        TerraJumpLeft2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpLeft1, TerraJumpLeft2] timePerFrame:0.2]];
        
        //Jump action Right
        SKTexture* TerraJumpRight1 = [SKTexture textureWithImageNamed:@"TerraJumpRight1.gif"];
        TerraJumpRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraJumpRight2 = [SKTexture textureWithImageNamed:@"TerraJumpRight2.gif"];
        TerraJumpRight2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpRight1, TerraJumpRight2] timePerFrame:0.2]];
        
        //Victory Wave
        SKTexture* TerraWave1 = [SKTexture textureWithImageNamed:@"TerraWave1.tiff"];
        TerraWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraWave2 = [SKTexture textureWithImageNamed:@"TerraWave2.tiff"];
        TerraWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWave1, TerraWave2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* TerraTalk1 = [SKTexture textureWithImageNamed:@"TerraTalkRight1.tiff"];
        TerraTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraTalk2 = [SKTexture textureWithImageNamed:@"TerraTalkRight2.tiff"];
        TerraTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraTalk1, TerraTalk2] timePerFrame:0.2]];
        
        //Laugh
        SKTexture* TerraLaugh1 = [SKTexture textureWithImageNamed:@"TerraLaugh1.tiff"];
        TerraLaugh1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraLaugh2 = [SKTexture textureWithImageNamed:@"TerraLaugh2.tiff"];
        TerraLaugh2.filteringMode = SKTextureFilteringNearest;
        
        playerLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraLaugh1, TerraLaugh2] timePerFrame:0.2]];
        
        //Death
        playerHit = [SKTexture textureWithImageNamed:@"TerraHit1.gif"];
        playerHit.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraHit2 = [SKTexture textureWithImageNamed:@"TerraHit2.gif"];
        TerraHit2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* TerraHit3 = [SKTexture textureWithImageNamed:@"TerraHit3.gif"];
        TerraHit3.filteringMode = SKTextureFilteringNearest;
        
        playerDeath= [SKAction animateWithTextures:@[playerHit, TerraHit2,TerraHit3] timePerFrame:0.5];
        
        //Terra check
        
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }else{
        SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"CelesWalkRight1.tiff"];
        CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"CelesWalkRight2.tiff"];
        CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"CelesWalkRight3.tiff"];
        CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"CelesWalkRight4.tiff"];
        CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,  CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
        
        
        SKTexture* CelesWalkLeft1 = [SKTexture textureWithImageNamed:@"CelesWalkLeft1.tiff"];
        CelesWalkLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft2 = [SKTexture textureWithImageNamed:@"CelesWalkLeft2.tiff"];
        CelesWalkLeft2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft3 = [SKTexture textureWithImageNamed:@"CelesWalkLeft3.tiff"];
        CelesWalkLeft3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkLeft4 = [SKTexture textureWithImageNamed:@"CelesWalkLeft4.tiff"];
        CelesWalkLeft4.filteringMode = SKTextureFilteringNearest;
        
        
        playerWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkLeft1,  CelesWalkLeft2,CelesWalkLeft3,CelesWalkLeft4] timePerFrame:0.2]];
        
        playerRight = CelesWalkRight1;
        
        playerLeft = CelesWalkLeft1;
        
        //Jump action Left
        SKTexture* CelesJumpLeft1 = [SKTexture textureWithImageNamed:@"CelesJumpLeft1.tiff"];
        CelesJumpLeft1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesJumpLeft2 = [SKTexture textureWithImageNamed:@"CelesJumpLeft2.tiff"];
        CelesJumpLeft2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpLeft1, CelesJumpLeft2] timePerFrame:0.2]];
        
        //Jump action Right
        SKTexture* CelesJumpRight1 = [SKTexture textureWithImageNamed:@"CelesJumpRight1.tiff"];
        CelesJumpRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesJumpRight2 = [SKTexture textureWithImageNamed:@"CelesJumpRight2.tiff"];
        CelesJumpRight2.filteringMode = SKTextureFilteringNearest;
        
        playerJumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpRight1, CelesJumpRight2] timePerFrame:0.2]];
        
        //Victory Wave
        SKTexture* CelesWave1 = [SKTexture textureWithImageNamed:@"CelesVictory1.tiff"];
        CelesWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWave2 = [SKTexture textureWithImageNamed:@"CelesVictory2.tiff"];
        CelesWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWave1, CelesWave2] timePerFrame:0.2]];
        
        //Death
        playerHit = [SKTexture textureWithImageNamed:@"CelesDeath1.gif"];
        playerHit.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesDeath1 = [SKTexture textureWithImageNamed:@"CelesDeath2.gif"];
        CelesDeath1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesDeath2 = [SKTexture textureWithImageNamed:@"CelesDeath3.gif"];
        CelesDeath2.filteringMode = SKTextureFilteringNearest;
        
        playerDeath= [SKAction animateWithTextures:@[playerHit, CelesDeath1,CelesDeath2] timePerFrame:0.5];
        
        //Laugh
        SKTexture* CelesLaugh1 = [SKTexture textureWithImageNamed:@"CelesLaugh1.tiff"];
        CelesLaugh1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesLaugh2 = [SKTexture textureWithImageNamed:@"CelesLaugh2.tiff"];
        CelesLaugh2.filteringMode = SKTextureFilteringNearest;
        
        playerLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesLaugh1, CelesLaugh2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* CelesTalk1 = [SKTexture textureWithImageNamed:@"CelesTalk1.tiff"];
        CelesTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesTalk2 = [SKTexture textureWithImageNamed:@"CelesTalk2.tiff"];
        CelesTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesTalk1, CelesTalk2] timePerFrame:0.2]];
        
        //Celes check
        
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }
}


-(void) makeImpsPhase1{
    //Set up imo
    int impXpos[3];
    
    impXpos[0] = 0;
    impXpos[1] = 500;
    impXpos[2] = 1000;


    for(int i = 0; i < 3;i++){
        
        SKTexture* impStart;
        
        impStart = [SKTexture textureWithImageNamed:@"impleft1.tiff"];
        
        imp[i] = [SKSpriteNode spriteNodeWithTexture: impStart];
        
        [imp[i] setPosition: CGPointMake(impXpos[i],-517)];
        imp[i].size = CGSizeMake(80, 100);
        
        imp[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:imp[i].texture.size];
        imp[i].physicsBody.dynamic = YES;
        imp[i].physicsBody.allowsRotation = false;
        imp[i].physicsBody.categoryBitMask = enemyCategory;
        imp[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory;
        imp[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        imp[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"imp";
        NSString* impNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:impNum];
        
        imp[i].name = str;
        
        impDirectionLeft[i] = true;
        
        [self addChild: (imp[i])];
        
    }
}

-(void) makeImpsPhase2{
    //Set up imo
    int impXpos[6];
    
    impXpos[3] = 5300;
    impXpos[4] = 5800;
    impXpos[5] = 6300;
    
    
    for(int i = 3; i < 6;i++){
        
        SKTexture* impStart;
        
        impStart = [SKTexture textureWithImageNamed:@"impleft1.tiff"];
        
        imp[i] = [SKSpriteNode spriteNodeWithTexture: impStart];
        
        [imp[i] setPosition: CGPointMake(impXpos[i],-517)];
        imp[i].size = CGSizeMake(80, 100);
        
        imp[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:imp[i].texture.size];
        imp[i].physicsBody.dynamic = YES;
        imp[i].physicsBody.allowsRotation = false;
        imp[i].physicsBody.categoryBitMask = enemyCategory;
        imp[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory;
        imp[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        imp[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"imp";
        NSString* impNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:impNum];
        
        imp[i].name = str;
        
        impDirectionLeft[i] = true;
        
        [self addChild: (imp[i])];
        
    }
}


-(void) makeMage{
    //Set up imo
    int xPos[2];
    xPos[0] = 3800;
    xPos[1] = 8600;
    
    for(int i = 0; i < 2;i++){
        mageAlive[i] = true;
        SKTexture* mageStart;
        
        mageStart = [SKTexture textureWithImageNamed:@"Magician.gif"];
        
        mage[i] = [SKSpriteNode spriteNodeWithTexture: mageStart];
        //[imp[i] setTexture:impStart];
        [mage[i] setPosition: CGPointMake(xPos[i] - 50,1000)];
        
        
        mage[i].size = CGSizeMake(80, 120);
        
        mage[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:mage[i].texture.size];
        mage[i].physicsBody.dynamic = YES;
        mage[i].physicsBody.allowsRotation = false;
        mage[i].physicsBody.categoryBitMask = enemyCategory;
        mage[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory;
        mage[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        mage[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"mage";
        NSString* mageNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:mageNum];
        
        mage[i].name = str;
        
        [self addChild: (mage[i])];
        
        mageAlive[i] = true;
        
    }
}



-(void) makeEnemyAnimation{

    //Bad guy animation
    
    
    //Walk action Left
    SKTexture* ImpWalkLeft1 = [SKTexture textureWithImageNamed:@"impleft1.tiff"];
    ImpWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkLeft2 = [SKTexture textureWithImageNamed:@"impleft2.tiff"];
    ImpWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkLeft3 = [SKTexture textureWithImageNamed:@"impleft3.tiff"];
    ImpWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkLeft4 = [SKTexture textureWithImageNamed:@"impleft4.tiff"];
    ImpWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    impLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[ImpWalkLeft1,ImpWalkLeft2,ImpWalkLeft3,ImpWalkLeft4] timePerFrame:0.2]];
    
    
    //Walk action Right
    SKTexture* ImpWalkRight1 = [SKTexture textureWithImageNamed:@"impright1.tiff"];
    ImpWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkRight2 = [SKTexture textureWithImageNamed:@"impright2.tiff"];
    ImpWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkRight3 = [SKTexture textureWithImageNamed:@"impright3.tiff"];
    ImpWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkRight4 = [SKTexture textureWithImageNamed:@"impright4.tiff"];
    ImpWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    impRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[ImpWalkRight1,ImpWalkRight2,ImpWalkRight3,ImpWalkRight4] timePerFrame:0.2]];
    
}


-(void)makeFireBall{
    [fireBall removeFromParent];
    fireBall = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
    //Fireball to the left or right of the player
    if(playerFacingLeft == false){
        [fireBall setPosition: CGPointMake(player.position.x + 30,player.position.y +10)];
    }
    else{
        [fireBall setPosition: CGPointMake(player.position.x - 30,player.position.y + 10)];
    }
    fireBall.size = CGSizeMake(60, 60);
    
    fireBall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fireBall.texture.size];
    
    fireBall.physicsBody.affectedByGravity = false;
    fireBall.physicsBody.allowsRotation = false;
    fireBall.physicsBody.categoryBitMask = fireBallCategory;
    fireBall.physicsBody.contactTestBitMask = enemyCategory;
    fireBall.physicsBody.usesPreciseCollisionDetection = YES;
    [self addChild: fireBall];
}




-(void)makeMageFireBall{
    for(int i = 0; i < 2; i++){
        [mageFireBall[i] removeFromParent];
        if(mageAlive[i] == true){
            mageFireBall[i] = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
            //Fireball to the left or right of the player
            if(player.position.x < mage[i].position.x){
                [mageFireBall[i] setPosition: CGPointMake(mage[i].position.x ,mage[i].position.y +100)];
            }
            else{
                [mageFireBall[i] setPosition: CGPointMake(mage[i].position.x,mage[i].position.y + 100)];
            }
            mageFireBall[i].size = CGSizeMake(60, 60);
            
            mageFireBall[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:mageFireBall[i].texture.size];
            
            mageFireBall[i].physicsBody.affectedByGravity = false;
            mageFireBall[i].physicsBody.allowsRotation = false;
            mageFireBall[i].physicsBody.categoryBitMask = enemyProjCategory;
            mageFireBall[i].physicsBody.contactTestBitMask = playerCategory|groundCategory;
            mageFireBall[i].physicsBody.usesPreciseCollisionDetection = YES;
            
            NSString *str = @"mageFire";
            NSString* fireNum = [NSString stringWithFormat:@"%i", i];
            str = [str stringByAppendingString:fireNum];
            
            mageFireBall[i].name = str;
            
            [self addChild: mageFireBall[i]];
        }
    }
}


-(void) mageAttack{
    [self makeMageFireBall];
    int playerX = player.position.x;
    int playerY = player.position.y;
    SKAction *goToPlayer;
    for(int i = 0; i < 2; i++){
        if(mageAlive[i] == true){
            if(mage[i].position.x - 2000 < player.position.x && mage[i].position.x - 1000 > player.position.x){
                goToPlayer = [SKAction moveTo: CGPointMake(playerX, playerY) duration: 6];
                [mageFireBall[i] runAction:goToPlayer];
            }else if(mage[i].position.x - 1000 < player.position.x && mage[i].position.x - 500 > player.position.x){
                goToPlayer = [SKAction moveTo: CGPointMake(playerX, playerY) duration: 3];
                [mageFireBall[i] runAction:goToPlayer];
            }else if(mage[i].position.x - 500 < player.position.x ){
                goToPlayer = [SKAction moveTo: CGPointMake(playerX, playerY) duration: 1];
                [mageFireBall[i] runAction:goToPlayer];
            }
        }
    }
}


//fire button
- (void)makeFireButton
{
    fireButton = [SKSpriteNode spriteNodeWithImageNamed:@"fireButton.png"];
    fireButton.size = CGSizeMake(100 ,100);
    fireButton.position = CGPointMake(player.position.x,-620);
    fireButton.name = @"fireButtonNode";//how the node is identified later
    
    [self addChild: (fireButton)];
    
}

-(void) makeResetButton {
    resetButton = [SKSpriteNode spriteNodeWithImageNamed:@"reset.png"];
    resetButton.size = CGSizeMake(100 ,100);
    resetButton.position = CGPointMake(player.position.x + 270,-620);
    resetButton.name = @"reset";//how the node is identified later
    
    [self addChild: (resetButton)];
}


-(void) makeHealthLabel{
    healthLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    healthLabel.position = CGPointMake(player.position.x+300, 600);
    healthLabel.fontSize = 50;
    healthLabel.fontColor = [SKColor redColor];
    healthLabel.name = @"healthLabel";
    
    NSString *hpLeft = [NSString stringWithFormat:@"HP : %d",hitPoints];
    healthLabel.text = hpLeft;
    
    [self addChild:healthLabel];
    
    
}


-(void) makeDefeatLabel{
    defeatLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkboardSE-Bold"];
    defeatLabel.position = CGPointMake(player.position.x, player.position.y+400);
    defeatLabel.fontSize = 40;
    defeatLabel.fontColor = [SKColor redColor];
    defeatLabel.name = @"defeatLabel";
    
    NSString * gameOverText = [NSString stringWithFormat:@"You failed to save the princess!"];
    defeatLabel.text = gameOverText;
    
    [self addChild:defeatLabel];
    
    
}



-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    
    if ([node.name isEqualToString:@"fireButtonNode"] && hitPoints > 0) {
        [self makeFireBall];
        if(playerFacingLeft == true){
            [fireBall.physicsBody applyImpulse:CGVectorMake(-15, 0)];
        }
        else{
            [fireBall.physicsBody applyImpulse:CGVectorMake(15, 0)];
        }
    }
    
    else if ([node.name isEqualToString:@"reset"]) {
        [self reset];
    }
    
    else{
        
        if(hitPoints > 0){
            if(location.y <= player.position.y){
                //if(player.physicsBody.velocity.dx == 0){
                if(location.x > player.position.x && location.y){
                    [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
                    if(running == false || playerFacingLeft == true){
                        [player runAction:playerWalkRight];
                    }
                    running = true;
                    playerFacingLeft = false;
                    
                }
                else{
                    [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                    if(running == false || playerFacingLeft == false){
                        [player runAction:playerWalkLeft];
                    }
                    running = true;
                    
                    playerFacingLeft = true;
                    
                }
            }
            else{
                if(player.physicsBody.velocity.dy == 0){
                    if(location.x > player.position.x && location.y){
                        playerFacingLeft = false;
                        
                        [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
                        [player runAction:playerJumpRight];
                        running = false;
                        
                    }
                    else{
                        playerFacingLeft = true;
                        [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                        [player runAction:playerJumpLeft];
                        running = false;
                        
                    }
                }
            }
            
        }
     
    }
    
    
}

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask == enemyCategory)&&
        (secondBody.categoryBitMask == enemyCategory))
    {
        //Figure out how to unclog imps
    }
    
    if ((firstBody.categoryBitMask == enemyCategory)&& (secondBody.categoryBitMask == fireBallCategory))
    {
        // printf("Fireball hit");
        //NSLog(@"%@ \n" , firstBody.node.name);
        //How know what object to remove
        SKTexture* DeadImp = [SKTexture textureWithImageNamed:@"ImpHit.gif"];
        if([firstBody.node.name  isEqualToString: @"imp0"]){
            [imp[0] removeAllActions];
            [imp[0] setTexture:DeadImp];
            [imp[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"imp1"]){
            [imp[1] removeAllActions];
            [imp[1] setTexture:DeadImp];
            [imp[1] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"imp2"]){
            [imp[2] removeAllActions];
            [imp[2] setTexture:DeadImp];
            [imp[2] removeFromParent];
            
        }
        
        if([firstBody.node.name  isEqualToString: @"imp3"]){
            [imp[3] removeAllActions];
            [imp[3] setTexture:DeadImp];
            [imp[3] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"imp4"]){
            [imp[4] removeAllActions];
            [imp[4] setTexture:DeadImp];
            [imp[4] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"imp5"]){
            [imp[5] removeAllActions];
            [imp[5] setTexture:DeadImp];
            [imp[5] removeFromParent];
            
        }
        if([firstBody.node.name  isEqualToString: @"mage0"]){
            [mage[0] removeFromParent];
            mageAlive[0] = false;
            
        }
        if([firstBody.node.name  isEqualToString: @"mage1"]){
            [mage[1] removeFromParent];
            mageAlive[1] = false;
        }
         

        
        //Change to dead imp
        [fireBall removeFromParent];
        
        
    }
    
    if ((firstBody.categoryBitMask == enemyCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        //WHY IS THIS BREAKING NOT SURE
        //printf("%@ %@ \n", firstBody, secondBody);
        //NSLog(@" %@ %@ \n", firstBody, secondBody);
        //printf("Collison");
        hitPoints --;
        
        if (hitPoints > 0){
            [player removeAllActions];
            [player setTexture: playerHit];
        }
        
    }
    
    if ((firstBody.categoryBitMask == enemyProjCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        hitPoints --;
        //if(hitPoints <= 0){
        //gameOver
        //    [player removeAllActions];
        //    [player runAction:death];
        //}
        if(hitPoints > 0){
            [player removeAllActions];
            [player setTexture: playerHit];
            
        }
        
        
        if([firstBody.node.name  isEqualToString: @"mageFire0"]){
            [mageFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"mageFire1"]){
            [mageFireBall[1] removeFromParent];
        }

    }
    
    if ((firstBody.categoryBitMask == enemyProjCategory)&&
        (secondBody.categoryBitMask == groundCategory))
    {
        //printf("%@ %@ \n", firstBody, secondBody);
        
        if([firstBody.node.name  isEqualToString: @"mageFire0"]){
            [mageFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"mageFire1"]){
            [mageFireBall[1] removeFromParent];
        }
    }
     
    
    [[SharedData sharedInstance] setPlayerHealth:hitPoints];
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    hitPoints = [[SharedData sharedInstance] playerHealth];

    cam.position = CGPointMake(player.position.x, CGRectGetMidY(self.frame));
    
    //Keep button on player
    fireButton.position = CGPointMake(player.position.x, -620);
    
    resetButton.position = CGPointMake(player.position.x + 270, -620);
    
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    
    
    printf("Player x : %f y : %f \n",player.position.x,player.position.y);
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    if(player.physicsBody.velocity.dx == 0 && player.physicsBody.velocity.dy == 0 && hitPoints > 0){
        [player removeAllActions];
        if(player.position.x < 20 && player.position.x > -20){
            running =false;
            [player setTexture: playerIdle];
        }
        else{
            if(playerFacingLeft == true){
                running =false;
                [player setTexture: playerLeft];
            }else{
                running =false;

                [player setTexture:playerRight];
            }
        }
    }
    
    if(hitPoints >= 0){
        NSString *hpLeft = [NSString stringWithFormat:@"HP : %d",hitPoints];
        healthLabel.text = hpLeft;
    }
    
    healthLabel.position = CGPointMake(player.position.x+250, 600);
    defeatLabel.position = CGPointMake(player.position.x, player.position.y+400);

    
    //Actions based on time
    
    
    
    if(hitPoints == 0){
        [player removeAllActions];
        [player runAction:playerDeath];
        [self makeDefeatLabel];
        hitPoints = -1;
    }
    
    
    //Below screen
    if(player.position.y < -650){
        hitPoints--;
        if(hitPoints > 0) {
            [player setPosition:CGPointMake(checkPointX, checkPointY)];
            [backBorder setPosition: CGPointMake(player.position.x - 1000,player.position.y)];
            running =false;
        }else{
            [player removeFromParent];
        }
    }

    //UpdateBorder Here
    if(backBorder.position.x < player.position.x -1000){
        [backBorder setPosition: CGPointMake(player.position.x - 1000,player.position.y)];
    }
    
    //Update Checkpoints here
    if(player.position.x > 4700 && player.position.x < 5000 && secondPhaseSpawn == false){
        checkPointX = player.position.x;
        checkPointY = player.position.y +20;
        [backBorder setPosition: CGPointMake(4600,player.position.y)];
        //At this point we can clear anything before this point
        [self cleanUp:4600];
        [self makeImpsPhase2];
        secondPhaseSpawn = true;
        
    }
    
    if(player.position.x > 10000 && player.position.x < 10300){
        checkPointX = player.position.x;
        checkPointY = player.position.y +20;
        [backBorder setPosition: CGPointMake(9500,player.position.y)];
        //At this point we can clear anything before this point
        [self cleanUp:9500];
    }
    
    if(player.position.x > 10700 && 11000 > player.position.x){
        [player removeAllActions];
        [player runAction:playerVictory];
        printf("BossFight but for now victory move onto next stage");
        GKScene *scene = [GKScene sceneWithFileNamed:@"TowerScene"];
        
        SKTransition *reveal = [SKTransition fadeWithDuration:1.5];
        TowerScene * towerScene = (TowerScene *)scene.rootNode;
        
        towerScene.entities = [scene.entities mutableCopy];
        towerScene.graphs = [scene.graphs mutableCopy];
        towerScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:towerScene transition: reveal];
        
    }
    
    //Enemy movement and attacks
    if(player.position.x < 4700){
        int yJump;
        int xSpeed;
        for(int i = 0; i < 3; i++){
            yJump = arc4random_uniform(15);
            xSpeed =  arc4random_uniform(15);
            if(imp[i].physicsBody.velocity.dx == 0 && impDirectionLeft[i] == true){
                
                [imp[i].physicsBody applyImpulse:CGVectorMake(-1 * xSpeed, yJump)];
                [imp[i] runAction:impLeft];
                
            }
            else if(imp[i].physicsBody.velocity.dx == 0 && impDirectionLeft[i] == false){
                [imp[i].physicsBody applyImpulse:CGVectorMake(xSpeed, yJump)];
                [imp[i] runAction:impRight];
                
            }
            
            if(imp[i].position.x < -1000 || imp[i].position.x - 100 < backBorder.position.x){
                impDirectionLeft[i] = false;
            }
            if(imp[i].position.x > 1350){
                impDirectionLeft[i] = true;
            }

        }

    }
    
    if(player.position.x > 4700 && player.position.x < 10000){
        int yJump;
        int xSpeed;
        for(int i = 3; i < 6; i++){
            yJump = arc4random_uniform(15);
            xSpeed =  arc4random_uniform(15);
            if(imp[i].physicsBody.velocity.dx == 0 && impDirectionLeft[i] == true){
                
                [imp[i].physicsBody applyImpulse:CGVectorMake(-1 * xSpeed, yJump)];
                [imp[i] runAction:impLeft];
                
            }
            else if(imp[i].physicsBody.velocity.dx == 0 && impDirectionLeft[i] == false){
                [imp[i].physicsBody applyImpulse:CGVectorMake(xSpeed, yJump)];
                [imp[i] runAction:impRight];
                
            }
            
            if(imp[i].position.x < 5000 || imp[i].position.x - 100 < backBorder.position.x){
                impDirectionLeft[i] = false;
            }
            if(imp[i].position.x > 6400){
                impDirectionLeft[i] = true;
            }
            
        }
        
    }
    
    if(frameCounter == 30*6){
        [self mageAttack];
        frameCounter = 0;
    }
    
    
    [[SharedData sharedInstance] setPlayerHealth:hitPoints];

    _lastUpdateTime = currentTime;
    frameCounter++;
}

@end
