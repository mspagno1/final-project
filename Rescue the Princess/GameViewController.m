//
//  GameViewController.m
//  Rescue the Princess
//
//  Created by Vaster on 5/9/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "IntroScene.h"
#import "CharectorSelect.h"

SKView *skView;

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
    // including entities and graphs.
    GKScene *scene = [GKScene sceneWithFileNamed:@"CharectorSelect"];
    //GKScene *scene = [GKScene sceneWithFileNamed:@"GameScene"];
    //GKScene *scene = [GKScene sceneWithFileNamed:@"TowerScene"];
    //GKScene *scene = [GKScene sceneWithFileNamed:@"BossScene"];

    
    // Get the SKScene from the loaded GKScene
    CharectorSelect *sceneNode = (CharectorSelect *)scene.rootNode;
    //GameScene *sceneNode = (GameScene *)scene.rootNode;
    //TowerScene *sceneNode = (TowerScene *)scene.rootNode;
    //BossScene *sceneNode = (BossScene *)scene.rootNode;

    
    // Copy gameplay related content over to the scene
    sceneNode.entities = [scene.entities mutableCopy];
    sceneNode.graphs = [scene.graphs mutableCopy];
    
    // Set the scale mode to scale to fit the window
    sceneNode.scaleMode = SKSceneScaleModeAspectFill;
    
    skView = (SKView *)self.view;
    
    // Present the scene
    [skView presentScene:sceneNode];
    
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
}

- (BOOL)shouldAutorotate {
    return YES;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
