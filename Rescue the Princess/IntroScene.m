//
//  IntroScene.m
//  Rescue the Princess
//
//  Created by Vaster on 5/9/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "IntroScene.h"

@implementation IntroScene{

    NSTimeInterval _lastUpdateTime;

    SKSpriteNode * background;
    SKSpriteNode * ground;
    SKSpriteNode * player;
    SKSpriteNode * princess;
    SKSpriteNode * dragonBoss;
    
    //Think Labels

    SKLabelNode * thoughtLabel1;
    SKLabelNode * thoughtLabel2;
    SKLabelNode * thoughtLabel3;
    SKLabelNode * thoughtLabel4;
    SKLabelNode * thoughtLabel5;
    SKLabelNode * thoughtLabel6;

    SKSpriteNode * thoughtBack;
    
    
    //Think Labels
    
    SKLabelNode * talkLabel1;
    SKLabelNode * talkLabel2;
    SKLabelNode * talkLabel3;
    SKLabelNode * talkLabel4;
    SKLabelNode * talkLabel5;
    SKLabelNode * talkLabel6;
    
    SKSpriteNode * talkBack;

    //Player actions

    SKAction* walkRight;
    SKAction* playerVictory;
    SKAction* playerTalk;
    SKAction* playerCheck;

    SKTexture* playerRight;
    SKTexture* playerLeft;
    SKTexture* playerIdle;
    SKTexture* playerSadLeft;
    
    //Princess Actions
    
    SKAction * mariaWalkRight;
    SKAction * mariaWalkLeft;
    SKAction * mariaSing;
    
    SKTexture * mariaIdle;
    SKTexture * mariaRight;
    SKTexture * mariaLeft;

    //Boss Actions
    SKTexture * bossTiltRightUp;
    SKTexture * bossTiltRightDown;
    SKTexture * bossRight;
    
    SKTexture * bossTiltLeftUp;
    SKTexture * bossTiltLeftDown;
    SKTexture * bossLeft;
    
    //Random Values
    int frameCounter;
    

}

- (void)sceneDidLoad {
    // Setup your scene here
    
    frameCounter = 0;
    static BOOL didLoad = NO;
    
    
    if (didLoad)
    {
        return;
    }
    didLoad = YES;
    
    // Initialize update time
    _lastUpdateTime = 0;
    
    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );
    
    
    [self makeBackground];
    [self makeGround];
    [self makePlayer];
    [self makePrincess];
    [self makePlayerAnimation];
    [self makePrincessAnimation];
    [self makeBossAnimation];
    //[player runAction:terraTalk];

    
    
}

-(void) makeBackground{
    
    
    background = [SKSpriteNode spriteNodeWithImageNamed: @"background.png"];
    background.size = CGSizeMake(self.frame.size.width ,self.frame.size.height);
        
    [background setPosition: CGPointMake(0,0)];
    background.physicsBody.affectedByGravity = FALSE;
    [self addChild: (background)];
    
    
    
}

-(void) makeGround{
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    ground.size = CGSizeMake(ground.texture.size.width * 1.4, ground.texture.size.height);
    
    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(ground.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    
    [self addChild: (ground)];
    
}



-(void) makePlayer{
    //Set up player
    
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        playerIdle = [SKTexture textureWithImageNamed:@"Terra.gif"];
    }else{
        playerIdle = [SKTexture textureWithImageNamed:@"Celes.gif"];
    }
    player = [SKSpriteNode spriteNodeWithTexture: playerIdle];
    [player setPosition: CGPointMake(-300,-517)];
    player.size = CGSizeMake(80, 100);
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    
    [self addChild: (player)];
}

-(void) makePrincess{
    //Set up player
    mariaIdle = [SKTexture textureWithImageNamed:@"Maria.gif"];
    princess = [SKSpriteNode spriteNodeWithTexture: mariaIdle];
    [princess setPosition: CGPointMake(300,-517)];
    princess.size = CGSizeMake(80, 100);
    
    princess.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:princess.texture.size];
    princess.physicsBody.dynamic = YES;
    princess.physicsBody.allowsRotation = false;
    
    [self addChild: (princess)];
}


-(void) makeBoss{
    //Set up player
    SKTexture *bossSpawn = [SKTexture textureWithImageNamed:@"BossRight.gif"];
    dragonBoss = [SKSpriteNode spriteNodeWithTexture: bossSpawn];
    [dragonBoss setPosition: CGPointMake(-300,300)];
    dragonBoss.size = CGSizeMake(400, 300);
    
    dragonBoss.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:dragonBoss.texture.size];
    dragonBoss.physicsBody.dynamic = NO;
    dragonBoss.physicsBody.allowsRotation = false;
    dragonBoss.physicsBody.affectedByGravity = FALSE;

    
    [self addChild: (dragonBoss)];
}



-(void) makePlayerAnimation{
    
    int charNum = [[SharedData sharedInstance] characterNumber];
    if(charNum == 0){
        //Walk action Right
        SKTexture* TerraWalkRight1 = [SKTexture textureWithImageNamed:@"TerraWalkRight1.gif"];
        TerraWalkRight1.filteringMode = SKTextureFilteringNearest;
    
        SKTexture* TerraWalkRight2 = [SKTexture textureWithImageNamed:@"TerraWalkRight2.gif"];
        TerraWalkRight2.filteringMode = SKTextureFilteringNearest;
    
        SKTexture* TerraWalkRight3 = [SKTexture textureWithImageNamed:@"TerraWalkRight3.gif"];
        TerraWalkRight3.filteringMode = SKTextureFilteringNearest;
    
        SKTexture* TerraWalkRight4 = [SKTexture textureWithImageNamed:@"TerraWalkRight4.gif"];
        TerraWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
        walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkRight1,  TerraWalkRight2,TerraWalkRight3,TerraWalkRight4] timePerFrame:0.2]];
    
        playerRight = TerraWalkRight1;
    
        playerLeft = [SKTexture textureWithImageNamed:@"TerraWalkLeft1.gif"];
        
        playerSadLeft = [SKTexture textureWithImageNamed:@"TerraSadLeft.gif"];
    
    
        //Victory Wave
        SKTexture* TerraWave1 = [SKTexture textureWithImageNamed:@"TerraWave1.tiff"];
        TerraWave1.filteringMode = SKTextureFilteringNearest;
    
        SKTexture* TerraWave2 = [SKTexture textureWithImageNamed:@"TerraWave2.tiff"];
        TerraWave2.filteringMode = SKTextureFilteringNearest;
    
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWave1, TerraWave2] timePerFrame:0.2]];
    
        //Talk
        SKTexture* TerraTalk1 = [SKTexture textureWithImageNamed:@"TerraTalkRight1.tiff"];
        TerraTalk1.filteringMode = SKTextureFilteringNearest;
    
        SKTexture* TerraTalk2 = [SKTexture textureWithImageNamed:@"TerraTalkRight2.tiff"];
        TerraTalk2.filteringMode = SKTextureFilteringNearest;
    
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraTalk1, TerraTalk2] timePerFrame:0.2]];
    
        //Terra check
    
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }else{
        SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"CelesWalkRight1.tiff"];
        CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"CelesWalkRight2.tiff"];
        CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"CelesWalkRight3.tiff"];
        CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"CelesWalkRight4.tiff"];
        CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
        
        
        walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,  CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
        
        playerRight = CelesWalkRight1;
        
        playerLeft = [SKTexture textureWithImageNamed:@"CelesWalkLeft1.tiff"];
        
        playerSadLeft = [SKTexture textureWithImageNamed:@"CelesSadLeft.gif"];

        
        
        //Victory Wave
        SKTexture* CelesWave1 = [SKTexture textureWithImageNamed:@"CelesVictory1.tiff"];
        CelesWave1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesWave2 = [SKTexture textureWithImageNamed:@"CelesVictory2.tiff"];
        CelesWave2.filteringMode = SKTextureFilteringNearest;
        
        playerVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWave1, CelesWave2] timePerFrame:0.2]];
        
        //Talk
        SKTexture* CelesTalk1 = [SKTexture textureWithImageNamed:@"CelesTalk1.tiff"];
        CelesTalk1.filteringMode = SKTextureFilteringNearest;
        
        SKTexture* CelesTalk2 = [SKTexture textureWithImageNamed:@"CelesTalk2.tiff"];
        CelesTalk2.filteringMode = SKTextureFilteringNearest;
        
        playerTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesTalk1, CelesTalk2] timePerFrame:0.2]];
        
        //Celes check
        
        playerCheck = [SKAction repeatActionForever:[SKAction animateWithTextures:@[playerLeft,playerRight] timePerFrame:0.3]];
    }

    

}

-(void) makePrincessAnimation{
    
    
    //Walk action Right
    SKTexture* MariaWalkLeft1 = [SKTexture textureWithImageNamed:@"MariaWalkLeft1.tiff"];
    MariaWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaWalkLeft2 = [SKTexture textureWithImageNamed:@"MariaWalkLeft2.tiff"];
    MariaWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaWalkLeft3 = [SKTexture textureWithImageNamed:@"MariaWalkLeft3.tiff"];
    MariaWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaWalkLeft4 = [SKTexture textureWithImageNamed:@"MariaWalkLeft4.tiff"];
    MariaWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    mariaWalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[MariaWalkLeft1,MariaWalkLeft2,MariaWalkLeft3,MariaWalkLeft4] timePerFrame:0.2]];
    
    mariaLeft = [SKTexture textureWithImageNamed:@"MariaStandLeft.gif"];

    mariaRight = [SKTexture textureWithImageNamed:@"MariaStandRight.gif"];
    
    
    
    
    //Sing
    SKTexture* MariaSing1 = [SKTexture textureWithImageNamed:@"MariaSing1.tiff"];
    MariaSing1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaSing2 = [SKTexture textureWithImageNamed:@"MariaSing2.tiff"];
    MariaSing2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaSing3 = [SKTexture textureWithImageNamed:@"MariaSing3.tiff"];
    MariaSing3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* MariaSing4 = [SKTexture textureWithImageNamed:@"MariaSing4.tiff"];
    MariaSing4.filteringMode = SKTextureFilteringNearest;
    
    mariaSing = [SKAction repeatActionForever:[SKAction animateWithTextures:@[MariaSing1, MariaSing2,MariaSing3, MariaSing4] timePerFrame:0.4]];
    
}


-(void) makeBossAnimation{
    
    //Boss
    
    bossLeft = [SKTexture textureWithImageNamed:@"BossLeft.gif"];
    bossTiltLeftUp = [SKTexture textureWithImageNamed:@"BossLeftTiltUp.gif"];
    bossTiltLeftDown = [SKTexture textureWithImageNamed:@"BossLeftTiltDown.gif"];
    bossRight = [SKTexture textureWithImageNamed:@"BossRight.gif"];
    bossTiltRightUp = [SKTexture textureWithImageNamed:@"BossRightTiltUp.gif"];
    bossTiltLeftDown = [SKTexture textureWithImageNamed:@"BossRightTiltDown.gif"];
    
}



-(void) makeThinkLabel{
    thoughtLabel1 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    thoughtLabel1.text = @"No more chickening out,";
    thoughtLabel1.fontColor = [UIColor blackColor];
    thoughtLabel1.fontSize = 18;
    
    thoughtLabel2 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    thoughtLabel2.text = @"I got this. I mean come on";
    thoughtLabel2.fontColor = [UIColor blackColor];
    thoughtLabel2.fontSize = 18;
    
    thoughtLabel3 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    thoughtLabel3.text = @"I've defeated dragons, evil warlords";
    thoughtLabel3.fontColor = [UIColor blackColor];
    thoughtLabel3.fontSize = 18;
    
    thoughtLabel4 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    thoughtLabel4.text = @"saving the kingdom countless times.";
    thoughtLabel4.fontColor = [UIColor blackColor];
    thoughtLabel4.fontSize = 18;
    
    thoughtLabel5 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    thoughtLabel5.text = @"How hard is asking one princess";
    thoughtLabel5.fontColor = [UIColor blackColor];
    thoughtLabel5.fontSize = 18;
    
    thoughtLabel6 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    thoughtLabel6.text = @"out, compared to that?";
    thoughtLabel6.fontColor = [UIColor blackColor];
    thoughtLabel6.fontSize = 18;
    
    
    SKTexture * thought = [SKTexture textureWithImageNamed:@"think"];
    thoughtBack = [SKSpriteNode spriteNodeWithTexture: thought];
    thoughtBack.size =  CGSizeMake(thoughtBack.texture.size.width * 2.5, thoughtBack.texture.size.height * 2.5);
    thoughtBack.position = CGPointMake(0, player.position.y+300);
    [thoughtBack addChild:thoughtLabel1];
    [thoughtBack addChild:thoughtLabel2];
    [thoughtBack addChild:thoughtLabel3];
    [thoughtBack addChild:thoughtLabel4];
    [thoughtBack addChild:thoughtLabel5];
    [thoughtBack addChild:thoughtLabel6];

    
    thoughtLabel1.position = CGPointMake(0, -thoughtLabel1.frame.size.height/2 + 45);
    thoughtLabel2.position = CGPointMake(0, -thoughtLabel2.frame.size.height/2 + 30);

    thoughtLabel3.position = CGPointMake(0, -thoughtLabel3.frame.size.height/2 + 15);
    thoughtLabel4.position = CGPointMake(0, -thoughtLabel3.frame.size.height/2 + 0);
    thoughtLabel5.position = CGPointMake(0, -thoughtLabel3.frame.size.height/2 - 15);
    thoughtLabel6.position = CGPointMake(0, -thoughtLabel3.frame.size.height/2 - 30);


    [self addChild:thoughtBack];
}


-(void) makeTalkLabel{
    talkLabel1 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    talkLabel1.text = @"Uh ,Greetings princess Maria.";
    talkLabel1.fontColor = [UIColor blackColor];
    talkLabel1.fontSize = 18;
    
    talkLabel2 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    talkLabel2.text = @"I was just wondering";
    talkLabel2.fontColor = [UIColor blackColor];
    talkLabel2.fontSize = 18;
    
    talkLabel3 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    talkLabel3.text = @"if you are not doing any";
    talkLabel3.fontColor = [UIColor blackColor];
    talkLabel3.fontSize = 18;
    
    talkLabel4 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    talkLabel4.text = @"tommorow, would you want to";
    talkLabel4.fontColor = [UIColor blackColor];
    talkLabel4.fontSize = 18;
    
    talkLabel5 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    talkLabel5.text = @"go to the bar or ";
    talkLabel5.fontColor = [UIColor blackColor];
    talkLabel5.fontSize = 18;
    
    talkLabel6 = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    talkLabel6.text = @"something...?";
    talkLabel6.fontColor = [UIColor blackColor];
    talkLabel6.fontSize = 18;
    
    
    SKTexture * talk = [SKTexture textureWithImageNamed:@"talk.png"];
    talkBack = [SKSpriteNode spriteNodeWithTexture: talk];
    talkBack.size =  CGSizeMake(talkBack.texture.size.width * 1, talkBack.texture.size.height * 1);
    talkBack.position = CGPointMake(0 -100, player.position.y+300);
    [talkBack addChild:talkLabel1];
    [talkBack addChild:talkLabel2];
    [talkBack addChild:talkLabel3];
    [talkBack addChild:talkLabel4];
    [talkBack addChild:talkLabel5];
    [talkBack addChild:talkLabel6];
    
    
    talkLabel1.position = CGPointMake(0, -talkLabel1.frame.size.height/2 + 75);
    talkLabel2.position = CGPointMake(0, -talkLabel2.frame.size.height/2 + 60);
    
    talkLabel3.position = CGPointMake(0, -talkLabel3.frame.size.height/2 + 45);
    talkLabel4.position = CGPointMake(0, -talkLabel4.frame.size.height/2 + 30);
    talkLabel5.position = CGPointMake(0, -talkLabel5.frame.size.height/2 + 15);
    talkLabel6.position = CGPointMake(0, -talkLabel6.frame.size.height/2 + 0);
    
    
    [self addChild:talkBack];
}


-(void) removeAllThoughtLabels{
    [thoughtLabel1 removeFromParent];
    [thoughtLabel2 removeFromParent];
    [thoughtLabel3 removeFromParent];
    [thoughtLabel4 removeFromParent];
    [thoughtLabel5 removeFromParent];
    [thoughtLabel6 removeFromParent];
    
    [thoughtBack removeFromParent];
}


-(void) removeAllTalkLabels{
    [talkLabel1 removeFromParent];
    [talkLabel2 removeFromParent];
    [talkLabel3 removeFromParent];
    [talkLabel4 removeFromParent];
    [talkLabel5 removeFromParent];
    [talkLabel6 removeFromParent];
    
    [talkBack removeFromParent];
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    if(frameCounter == 0){
        [self makeThinkLabel];
        [princess runAction:mariaSing];

    }
    
    if(frameCounter == 30*4){
        [self removeAllThoughtLabels];
        [player runAction:[SKAction moveTo:CGPointMake(0,-517) duration: 2]];
        [player runAction:walkRight];
    }
    if(player.position.x == 0 && frameCounter < 30*7){
        [player removeAllActions];
        [player setTexture:playerRight];
    }
    
    if(frameCounter == 30*7){
        [player removeAllActions];
        [player runAction:playerTalk];
        [princess removeAllActions];
        [princess setTexture:mariaLeft];
        
        [self makeTalkLabel];

    }
    
    if(frameCounter == 30 *12){
        [self removeAllTalkLabels];

        [player removeAllActions];
        [player setTexture:playerSadLeft];
        
        thoughtLabel1.text = @"Oh god I sounded so stupid,";
        thoughtLabel2.text = @"I can't look.";
        thoughtLabel3.text = @"Maybe I should run now before";
        thoughtLabel4.text = @"she laughes at me.";
        [thoughtBack addChild:thoughtLabel1];
        [thoughtBack addChild:thoughtLabel2];
        [thoughtBack addChild:thoughtLabel3];
        [thoughtBack addChild:thoughtLabel4];
        [self addChild:thoughtBack];
        
        
    }
    if(frameCounter == 30 * 13){
        [self makeBoss];
        [dragonBoss runAction:[SKAction moveTo:CGPointMake(princess.position.x,princess.position.y+150) duration: 2]];
        [dragonBoss setTexture:bossTiltLeftDown];
    }
    
    if(dragonBoss.position.x == princess.position.x){
        [dragonBoss setTexture:bossTiltRightUp];
        [dragonBoss runAction:[SKAction moveTo:CGPointMake(1000,400) duration: 2]];
        [princess runAction:[SKAction moveTo:CGPointMake(1000,400) duration: 2]];

    }
    
    if(frameCounter == 30 * 16){
        
        talkLabel1.text = @"Princess?";
        talkLabel2.text = @"Where did you go?";
        [talkBack addChild:talkLabel1];
        [talkBack addChild:talkLabel2];
        [self addChild:talkBack];
        
        [self removeAllThoughtLabels];
        [princess removeFromParent];
        [dragonBoss removeFromParent];
        [player runAction:playerCheck];
    }
    
    if(frameCounter == 30 * 18){
        talkLabel1.text = @"Oh shoot. Im ";
        talkLabel2.text = @"coming your majesty!";
        [player removeAllActions];
        [player runAction:walkRight];
        [player runAction:[SKAction moveTo:CGPointMake(400,-517) duration: 1]];
 
    }
    
    if(player.position.x > 399){
        [player removeFromParent];
        GKScene *scene = [GKScene sceneWithFileNamed:@"GameScene"];
        
        SKTransition *reveal = [SKTransition fadeWithDuration:1.5];
        GameScene * gameScene = (GameScene *)scene.rootNode;
        
        gameScene.entities = [scene.entities mutableCopy];
        gameScene.graphs = [scene.graphs mutableCopy];
        gameScene.scaleMode = SKSceneScaleModeAspectFill;
        
        [self.view presentScene:gameScene transition: reveal];

    }
    
    _lastUpdateTime = currentTime;
    frameCounter++;
}


@end
