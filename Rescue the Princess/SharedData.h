//
//  SharedData.h
//  Rescue the Princess
//
//  Created by Vaster on 5/10/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#ifndef SharedData_h
#define SharedData_h


#import <Foundation/Foundation.h>

@interface SharedData : NSObject

+(SharedData *)sharedInstance;
@property (nonatomic) int characterNumber;
@property (nonatomic) int playerHealth;


@end


#endif /* SharedData_h */
