//
//  GameViewController.h
//  Rescue the Princess
//
//  Created by Vaster on 5/9/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>
#import "SharedData.h"


@interface GameViewController : UIViewController

@end
