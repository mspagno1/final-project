//
//  IntroScene.h
//  Rescue the Princess
//
//  Created by Vaster on 5/9/17.
//  Copyright © 2017 Vaster. All rights reserved.
//


#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>
#import "SharedData.h"
#import "GameScene.h"

@interface IntroScene : SKScene

@property (nonatomic) NSMutableArray<GKEntity *> *entities;
@property (nonatomic) NSMutableDictionary<NSString*, GKGraph *> *graphs;

@end
