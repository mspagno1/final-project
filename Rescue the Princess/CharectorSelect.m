//
//  CharectorSelect.m
//  Rescue the celes
//
//  Created by Vaster on 5/10/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "CharectorSelect.h"

@implementation CharectorSelect{
    
    NSTimeInterval _lastUpdateTime;
    
    SKSpriteNode * background;
    SKSpriteNode * ground;
    
    SKSpriteNode * terra;
    SKSpriteNode * celes;
    
    
    //Labels
    SKLabelNode * chooseCharector;
    SKLabelNode * terraLabel;
    SKLabelNode * celesLabel;
    SKSpriteNode * charBack;
    SKSpriteNode * terraBack;
    SKSpriteNode * celesBack;
    
    SKSpriteNode * terraButton;
    SKSpriteNode * celesButton;

    
    
    //Terra actions
    
    SKAction* terraVictory;
    SKAction* terraTalk;
    SKAction* terraLaugh;
    SKAction * terraWink;
    
    SKTexture* terraIdle;
    
    //Celes actions
    
    SKAction* celesTalk;
    SKAction* celesLaugh;
    SKAction * celesVictory;
    SKAction* celesWink;
    
    SKTexture* celesIdle;
    
    //Random Values
    int frameCounter;
    
    
}

- (void)sceneDidLoad {
    // Setup your scene here
    frameCounter = 0;
    static BOOL didLoad = NO;
    
    
    if (didLoad)
    {
        return;
    }
    didLoad = YES;
    
    // Initialize update time
    _lastUpdateTime = 0;
    
    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );
    
    
    [self makeBackground];
    [self makeGround];
    [self makeTerra];
    [self makeCeles];
    [self makeTerraAnimation];
    [self makeCelesAnimation];
    [self makeCharectorSelect];
    [self makeTerraLabel];
    [self makeCelesLabel];
    
    
    
}

-(void) makeBackground{
    
    
    background = [SKSpriteNode spriteNodeWithImageNamed: @"background.png"];
    background.size = CGSizeMake(self.frame.size.width ,self.frame.size.height);
    
    [background setPosition: CGPointMake(0,0)];
    background.physicsBody.affectedByGravity = FALSE;
    [self addChild: (background)];
    
    
    
}

-(void) makeGround{
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    ground.size = CGSizeMake(ground.texture.size.width * 1.4, ground.texture.size.height);
    
    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(ground.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    
    [self addChild: (ground)];
    
}



-(void) makeTerra{
    //Set up player
    terraIdle = [SKTexture textureWithImageNamed:@"Terra.gif"];
    terra = [SKSpriteNode spriteNodeWithTexture: terraIdle];
    [terra setPosition: CGPointMake(-150,-517)];
    terra.size = CGSizeMake(80, 100);
    
    terra.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:terra.texture.size];
    terra.physicsBody.dynamic = YES;
    terra.physicsBody.allowsRotation = false;
    
    [self addChild: (terra)];
}

-(void) makeCeles{
    //Set up player
    celesIdle = [SKTexture textureWithImageNamed:@"Celes.gif"];
    celes = [SKSpriteNode spriteNodeWithTexture: celesIdle];
    [celes setPosition: CGPointMake(150,-517)];
    celes.size = CGSizeMake(80, 100);
    
    celes.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:celes.texture.size];
    celes.physicsBody.dynamic = YES;
    celes.physicsBody.allowsRotation = false;
    
    [self addChild: (celes)];
}




-(void) makeTerraAnimation{
    
    
    
    //Victory Wave
    SKTexture* TerraWave1 = [SKTexture textureWithImageNamed:@"TerraWave1.tiff"];
    TerraWave1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWave2 = [SKTexture textureWithImageNamed:@"TerraWave2.tiff"];
    TerraWave2.filteringMode = SKTextureFilteringNearest;
    
    terraVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWave1, TerraWave2] timePerFrame:0.2]];
    
    //Talk
    SKTexture* TerraTalk1 = [SKTexture textureWithImageNamed:@"TerraTalkRight1.tiff"];
    TerraTalk1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraTalk2 = [SKTexture textureWithImageNamed:@"TerraTalkRight2.tiff"];
    TerraTalk2.filteringMode = SKTextureFilteringNearest;
    
    terraTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraTalk1, TerraTalk2] timePerFrame:0.2]];
    

    SKTexture* TerraLaugh1 = [SKTexture textureWithImageNamed:@"TerraLaugh1.tiff"];
    TerraLaugh1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraLaugh2 = [SKTexture textureWithImageNamed:@"TerraLaugh2.tiff"];
    TerraLaugh2.filteringMode = SKTextureFilteringNearest;
    
    terraLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraLaugh1, TerraLaugh2] timePerFrame:0.2]];
    
    
    SKTexture* TerraWink1 = [SKTexture textureWithImageNamed:@"TerraWink1.tiff"];
    TerraWink1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWink2 = [SKTexture textureWithImageNamed:@"TerraWink2.tiff"];
    TerraWink2.filteringMode = SKTextureFilteringNearest;
    
    terraWink = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWink1, TerraWink2] timePerFrame:0.5]];
    
    
}

-(void) makeCelesAnimation{
    
    
    SKTexture* CelesLaugh1 = [SKTexture textureWithImageNamed:@"CelesLaugh1.tiff"];
    CelesLaugh1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesLaugh2 = [SKTexture textureWithImageNamed:@"CelesLaugh2.tiff"];
    CelesLaugh2.filteringMode = SKTextureFilteringNearest;
    
    celesLaugh = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesLaugh1, CelesLaugh2] timePerFrame:0.2]];
    
    //Talk
    SKTexture* CelesTalk1 = [SKTexture textureWithImageNamed:@"CelesTalk1.tiff"];
    CelesTalk1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesTalk2 = [SKTexture textureWithImageNamed:@"CelesTalk2.tiff"];
    CelesTalk2.filteringMode = SKTextureFilteringNearest;
    
    celesTalk = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesTalk1, CelesTalk2] timePerFrame:0.2]];

    //Victory Wave
    SKTexture* CelesWave1 = [SKTexture textureWithImageNamed:@"CelesVictory1.tiff"];
    CelesWave1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWave2 = [SKTexture textureWithImageNamed:@"CelesVictory2.tiff"];
    CelesWave2.filteringMode = SKTextureFilteringNearest;
    
    celesVictory = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWave1, CelesWave2] timePerFrame:0.2]];
    
    SKTexture* CelesWink1 = [SKTexture textureWithImageNamed:@"CelesWink1.tiff"];
    CelesWink1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWink2 = [SKTexture textureWithImageNamed:@"CelesWink2.tiff"];
    CelesWink2.filteringMode = SKTextureFilteringNearest;
    
    celesWink = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWink1, CelesWink2] timePerFrame:0.4]];
    
}


-(void) makeCharectorSelect{
    chooseCharector = [[SKLabelNode alloc]initWithFontNamed:@"Courier"];
    chooseCharector.text = @"Choose your charactor!";
    chooseCharector.fontColor = [UIColor blackColor];
    chooseCharector.fontSize = 25;
    
    
    charBack =  [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(chooseCharector.frame.size.width, chooseCharector.frame.size.height)];;
    
    charBack.size =  CGSizeMake(400, 100);
    charBack.position = CGPointMake(0, 0);
    [charBack addChild:chooseCharector];

    chooseCharector.position = CGPointMake(0, -chooseCharector.frame.size.height/2);

    [self addChild:charBack];
}

-(void) makeTerraLabel{
    terraLabel = [[SKLabelNode alloc]initWithFontNamed:@"Papyrus"];
    terraLabel.text = @"Terra";
    terraLabel.fontColor = [UIColor greenColor];
    terraLabel.fontSize = 25;
    
    
    terraBack =  [SKSpriteNode spriteNodeWithColor:[UIColor darkTextColor] size:CGSizeMake(terraLabel.frame.size.width, terraLabel.frame.size.height)];;
    
    terraBack.size =  CGSizeMake(100, 100);
    terraBack.position = CGPointMake(terra.position.x, terra.position.y + 200);
    [terraBack addChild:terraLabel];
    
    terraLabel.position = CGPointMake(0, -terraLabel.frame.size.height/2);
    
    [self addChild:terraBack];
    
    terraButton = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(terraBack.frame.size.width, terraBack.frame.size.height)];;
    terraButton.position = CGPointMake(terra.position.x, terra.position.y + 200);
    terraButton.name = @"terraButtonNode";//how the node is identified later
    
    [self addChild: (terraButton)];
}

-(void) makeCelesLabel{
    celesLabel = [[SKLabelNode alloc]initWithFontNamed:@"Papyrus"];
    celesLabel.text = @"Celes";
    celesLabel.fontColor = [UIColor yellowColor];
    celesLabel.fontSize = 25;
    
    
    celesBack =  [SKSpriteNode spriteNodeWithColor:[UIColor darkTextColor] size:CGSizeMake(celesLabel.frame.size.width, celesLabel.frame.size.height)];;
    
    celesBack.size =  CGSizeMake(100, 100);
    celesBack.position = CGPointMake(celes.position.x, celes.position.y + 200);
    [celesBack addChild:celesLabel];
    
    celesLabel.position = CGPointMake(0, -celesLabel.frame.size.height/2);
    
    [self addChild:celesBack];
    
    celesButton = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(celesBack.frame.size.width, celesBack.frame.size.height)];;
    celesButton.position = CGPointMake(celes.position.x, celes.position.y + 200);
    celesButton.name = @"celesButtonNode";//how the node is identified later
    
    [self addChild: (celesButton)];

}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    int charectorNum = -1;
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    if ([node.name isEqualToString:@"terraButtonNode"]) {
        //printf("Terra clicked here");
        charectorNum = 0;

    }else if([node.name isEqualToString:@"celesButtonNode"]){
        //printf("Celes clicked here");
        charectorNum = 1;
    }
    if(charectorNum == 0 || charectorNum == 1){
        [[SharedData sharedInstance] setCharacterNumber:charectorNum];
        //[[SharedData sharedInstance] setPlayerHealth:20];
        [[SharedData sharedInstance] setPlayerHealth:99];

    
        GKScene *scene = [GKScene sceneWithFileNamed:@"IntroScene"];
    
        SKTransition *reveal = [SKTransition fadeWithDuration:1.5];
        IntroScene * introScene = (IntroScene *)scene.rootNode;
    
        introScene.entities = [scene.entities mutableCopy];
        introScene.graphs = [scene.graphs mutableCopy];
        introScene.scaleMode = SKSceneScaleModeAspectFill;
    
        [self.view presentScene:introScene transition: reveal];
        //SKView * skView = (SKView *)self.view;
    
        // Present the scene
        //[skView presentScene:introScene];
    }

}



-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    if(frameCounter == 0){
        [terra runAction:terraLaugh];
        [celes runAction:celesLaugh];
    }
    
    if(frameCounter == 30 *3){
        [terra runAction:terraTalk];
        [celes runAction:celesTalk];
        
    }
    
    if(frameCounter == 30 *6){
        [terra runAction:terraVictory];
        [celes runAction:celesVictory];
        
    }
    
    if(frameCounter == 30 * 9){
        [terra runAction:terraWink];
        [celes runAction:celesWink];
        
    }
    
    if(frameCounter == 30 *11){
        frameCounter = -1;
    }
    
    _lastUpdateTime = currentTime;
    frameCounter++;
}



@end
